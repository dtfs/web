import { app } from '../../app/'
 
export const ui = (() => {
	'use strict'

	return {
		async init() {
			try {
				$(document).on('click', '.exit', async e => await app.user.exit())
			} catch(e) {
				app.error('user.ui.init')
			}
		},

		async update(page) {
			try {
				switch(page) {
					case 'home':
						const $phone = $('input.phone')
						const $form = $phone.closest('form')
						const $button = $form.find('button')
						const $code = $form.find('input.code')
						$form.on('change, submit', e => e.originalEvent.preventDefault())

						$phone.mask('+1 (000) 000-0000', {
							async onChange(e) {
								$phone.removeClass('error')
								$code.val('')
								$code.parent().hide()
							}, 
							async onComplete(e) {
								$button.button()

								await app.post('user', 'signin', {
									phone: $phone.cleanVal()
								}, async data => {
									$button.button(false)

									if(!data || data.error) {
										$phone.addClass('error')
										app.ui.alert(data.error)
										return
									}

									$code.parent().show().animateCss('fadeIn')
									$code.focus().mask('0000', {
										async onChange(e) {
											$button.button(false)
											$code.removeClass('error')
										},
										async onComplete(e) {
											$button.button()

											await app.post('user', 'signin', {
												phone: $phone.cleanVal(),
												code: $code.cleanVal()
											}, async data => {
												$button.button(false)

												if(!data || data.error) {
													$code.addClass('error')
													app.ui.alert(data.error)
													return
												}

												$('body').scrollTop(0)
												$('main').scrollTop(0)
												$('html').scrollTop(0)
												$(document).scrollTop(0)
												$('.container-fluid').scrollTop(0)
												app.storage.set('id', data.id)
												app.storage.set('session', data.session)
												app.storage.set('type', data.type)
												await app.user.init(data)
											})
										}
									})
								})
							}
						})
						break
				}
			} catch(e) {
				app.error('user.ui.update', e)
			}
		}
	}
})()
