import { app, VERSION } from '../app/'
 
export const user = (() => {
	'use strict'

	return {
		/* Main app init and all sub functions */
		async init(data = false) {
			await this.ui.init()

			const id =	data.id || app.storage.get('id') || false
			const session = data.session || app.storage.get('session') || false
			const type = data.type || app.storage.get('type') || false

			await app.post('user', 'session', { id, session, type }, async data => {
				if(!data || data.error) {
					await app.load('home')
					return
				}

				if(app.is.webview()) {
					await app.webkit.intercomInit()
					$(document).on('click', '#intercom', async e => {
						await app.webkit.intercomShow()
					})
				} else {
					window.Intercom('boot', {
						app_id: 'wsp8nf2a',
						phone: `+${data.data.phone}`,
						user_id: data.data.id,
						user_type: this.type(data.data.type),
					})
	 
					window.intercomSettings = { 
						app_id: 'wsp8nf2a',
						custom_launcher_selector: '#intercom'
					}
				}
				

				app.storage.set('me', JSON.stringify(data.data))
				app.settings = { data: data.data }
				
				await app.load(`me/${data.type}`, false, $page => {
					$page.find('.avatar')
						.css('background-image', `url(/dist/cache/avatar/${data.id % 26}.svg)`)
						.animateCss('bounceIn')
						.attr('data-popover', `${data.type}/menu`)
				})
			})
		},

		/* Main app init and all sub functions */
		async exit() {
			await app.socket.close()
			app.storage.remove('id')
			app.storage.remove('session')
			app.storage.remove('type')
			await app.load('home')
		},

		type(type = app.storage.get('type')) {
			switch(type) {
				case '1':
					return 'admin'; break;

				case '2':
					return 'master'; break;

				case '3':
					return 'vendor'; break;

				default: 
					return 'user'; break;
			}
		},

		async change(id, data = false, callback = false) {
			try {
				await app.post('user', 'change', {
					id, param: data.param, value: data.value
				}, async data => {
					if(callback)
						await callback(data)
				})
			} catch(e) {
				app.error(`master.change`, e)
			}
		}
	}
})()


import { ui } from './module/ui'
user.ui = ui
