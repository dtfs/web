import { app } from '../../app/'
 
const moment = require('moment')

export const planner = (() => {
	'use strict'

	return {
		async init() {
			try {
				await this.template.init()

				const $planner = $('.plan')
				const $map  = await app.map.init($('#map'))
				const $dates = $('.plan__days--dates')
				const $days  = $('.plan__days')
				const { pageDots, prevNextButtons } = false
				const cellAlign = app.is.mobile() ? 'center' : 'left'


				const me = app.settings.data 
				app.settings.marker = app.map.marker.create($map, {
					id: me.id,
					type: 'master',
					position: new google.maps.LatLng(me.lat, me.lng)
				})

				app.settings.marker.setMap($map)
				$map.setCenter(app.settings.marker.position)

				$planner.addClass('loading')
				/* START of DATES AND DAYS create */
				for (let i = 30; i > -30; i--) {
					let day = moment().add(i, 'days').format('MM/DD')
					let weekday = moment().add(i, 'days').format('dddd')
					let name = moment().add(i, 'days').format('dddd')

					switch(i) {
						case 1:
							name = 'Tomorrow'
							break

						case 0:
							name = 'Today'
							day = '<i data-feather="sun"></i>'
							break

						case -1:
							name = 'Yesterday'
							break
					}
 
					const $day = $(app.template('master/day')({ name }))
						.data('date', moment().add(i, 'days').format('YYYY-MM-DD'))
						.addClass(moment().add(i, 'days').format('YYYY-MM-DD'))
						.data('handle', i >= 0 ? true : false)

					const $date = $(i === 0 ? `<span style="width: 70px;">${day}</span>` : `<span>${day}</span>`)
						.data('day', $day)

					if(weekday === 'Sunday' || weekday == 'Saturday')
						$date.addClass('weekend')

					$dates.prepend($date)
					$days.prepend($day)
				}
				/* END of DATES AND DAYS create */
				app.ui.update()

				$dates.flickity({
					pageDots, prevNextButtons,
					cellAlign,
					asNavFor: $days[0],
					contain: true,
					freeScroll: true
				})

				let lastIndex = false
				$days.flickity({
					pageDots, prevNextButtons,
					cellAlign: 'left',
					draggable: false,
					adaptiveHeight: true,
					selectedAttraction: 0.2,
					friction: 0.8,
					on: {
				    async change(index) {
						WebuiPopovers.hideAll()
						const $day = $($('.day')[index])
						const $next = $($('.day')[index + 1])

						$day.find('.scrolling').addClass('loading')
						await app.request.load('day', $day.find('.orders'))
						app.map.direction.create($('.day.is-selected').find('.orders'))
						$('.day > .scrolling').removeClass('loading')

						if(index > lastIndex && !app.is.mobile()) {
							await app.request.load('day', $next.find('.orders'))
						}
					
						app.sound.click()
						lastIndex = index
				    }
				  }
				})


				await this.socket.connect()
				$days.flickity('select', 29)
				await app.request.load('day', $('.day .orders')[30])
				
				this.ui()

				setTimeout(() => {
					$planner.removeClass('loading')
					$('.days').flickity('resize')
				}, 1000)
			} catch(e) {
				app.error('planner.init', e)
			}
		},

		async change(param = false, value = false, additional = {}) {
			try {
				const $param = $(document).find(`[data-master-${param}]`) || false
				app.settings.data[param] = value
				
				switch(param) {
					case 'address':
						const marker = app.settings.marker
						await app.map.marker.position( marker, additional.lat, additional.lng )
						app.map.direction.create($('.day.is-selected').find('.orders'))
						break
				}


				if($param)
					$param.html(value)
			} catch(e) {
				app.error('master > change', e)
			}
		},

		/* Function desc */
		async ui($day = false) {
			try {
				if(!$day)
					$.each($('.day'), async (i, day) => {
						const $day = $(day)
						const $orders = $day.find('.orders')
						if($(day).data('handle')) {
							await muuri.init($orders)
						}
					})
			} catch(e) {
				app.error('planner.admin.ui', e)
			}
		}
	}
})()



import { socket } from './module/socket'
planner.socket = socket
import { request } from './module/request'
planner.request = request
import { muuri } from './module/muuri'
planner.muuri = muuri

import { template } from './module/template'
planner.template = template

import { popover } from './module/popover'
planner.popover = popover