import { app } from '../../../app/'
import { planner } from '../'

const moment = require('moment')
const feather = require('feather-icons')

export const request = (() => {
	'use strict'

	return {
		/* Function desc */
		async add($block = false, data) {
			try {
				const $map = $('#map').data('map') 
				data.status = app.request.status(parseInt(data.status))
				const $request = $(app.template(`${app.user.type()}/request`)(data))

				if($(`.order__${data.id}`).length > 0)
					$.each($(`.order__${data.id}`), (i,e) => this.remove($(e)))

				if($($block).data('muuri')) {
					$block = $($block).data('muuri')
					await $block.add([$request[0]], {index: data.position || 0})
				} else
					$($block).append($request)

				const $marker = app.map.marker.create($map, {
					id: data.id,
					type: data.status,
					position: new google.maps.LatLng(data.lat, data.lng)
				})
				$request.data('marker', $marker)
				$marker.setMap(null)

				await this.ui($request, data)
				await app.planner.muuri.update($request.closest('.muuri').data('muuri'))


				if($($block._element).closest('.day').hasClass('is-selected'))
					app.map.direction.create($($block._element))
			} catch(e) {
				app.error('request.add', e)
			}
		},

		/* Function REMOVE */
 		async remove($request) {
			try {
				const muuri = $request.closest('.muuri').data('muuri') || false
				const $orders = $request.closest('.orders')
				const isSelected = $request.closest('.day').hasClass('is-selected')
				
				$request.data('marker').setMap(null)
				$request.remove()

				await app.planner.muuri.update(muuri)

				if(isSelected)
					app.map.direction.create($orders)
			} catch(e) {
				app.error('request.remove', e)
			}
 		},


		/* Function desc */
		async ui($request, data) {
			try {
				feather.replace()
				$request.find('.badge.history').requestHistory()

				$.each($request.find('input.time'), (i, time) => {
					const $time = $(time)

					flatpickr(time, {
						enableTime: true,
						noCalendar: true,
						dateFormat: "h:i K",
						minuteIncrement: 15,
						defaultDate: $time.val(),
						async onOpen() {
							await app.ui.update()
							await app.sound.click()
						},
						async onChange(selectedDates, dateStr, instance) {
							if($time.hasClass('from')) {
								await app.request.change($request.data('id'), {
									param: 'start',
									value: dateStr
								})
							} else {
								await app.request.change($request.data('id'), {
									param: 'end',
									value: dateStr
								})
							}	
						}
					})
				})


				$request.find('.action.waiting').on('click', async e => {
					e.originalEvent.preventDefault()

					$.each($request.find('.action'), (i, action) => $(action).button())
					await app.request.change(data.id, {
						param: 'status',
						value: app.request.status($request.data('status')) + 1
					})
					$.each($request.find('.action'), (i, action) => $(action).button(false))
				})

				$request.find('.action.ready').on('click', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)
					const time = $this.data('time')

					$.each($request.find('.action'), (i, action) => $(action).button())
					await app.request.change(data.id, {
						param: 'status',
						value: app.request.status($request.data('status')) + 1,
						time
					})
					$.each($request.find('.action'), (i, action) => $(action).button(false))
				})
			} catch(e) {
				app.error('request.remove', e)
			}
		}

	}

})()
