import { app } from '../../../../app/'
 
export const popover = (() => {
   'use strict'
 
   return {
      async map($this, $content) {
         try { 
            await app.map.init($content.find('.map-ui'), async map => {
               const $orders = $this.closest('.day').find('.orders')
               const master = app.settings.data
               const marker = app.settings.marker
               
               if(marker) {
                  await marker.setMap(map)
                  await map.setCenter(marker.position)
               }

               app.map.direction.create($this, false, map, response => {
                  const route = []
                  $.each($orders.find('.order'), (i, order) => {
                     route.push($(order).data('address'))
                  })

                  const origin = master.address.replace(/ /g, '+')
                  let waypoints = ''
                  $.each(route, (i, address) => {
                     address = address.replace(/ /g, '+')
                     waypoints = `${waypoints}${address}`
                     waypoints = i === route.length - 1 ? `${waypoints}` : `${waypoints}/`
                  })

                  const link = `https://www.google.com/maps/dir/${origin}/${waypoints}/${origin}`
                  $content.find('.map--button').append(`
                     <a href="${link}" class="btn button large w-100" target="_blank">
                        Open in Google Maps
                     </a>
                  `)
               })
            })
         } catch(e) {
            app.error('popover > map', e)
         }
      }
   }
})()