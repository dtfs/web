import { templateLoader } from '../../../app/module/template'
import { app } from '../../../app/'
 
export const template = (() => {
   'use strict'

   return {
      async init() {
         try {
            await templateLoader.register({
               key: "master/day",
               path: "/client/template/master/day/index.html"
            })
            await templateLoader.register({
               key: "master/request",
               path: "/client/template/master/day/module/request/index.html"
            })

            await templateLoader.register({
               key: "master/request/cancel",
               path: "/client/template/master/day/module/request/module/cancel/index.html"
            })
         } catch(e) {

         }
      }
   }
})()