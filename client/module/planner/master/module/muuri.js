const Muuri = require('muuri')
import { app } from '../../../app/'
import { planner } from '../'


export const muuri = (() => {
 	'use strict'

 	return {
 		async init($block) {
 			try {
				if($block.data('muuri'))
					$block.data('muuri').destroy()

 				const muuri = new Muuri($block[0], {
					items: '.order',
					layoutOnResize: true
 				})

 				$block.data('muuri', muuri)
 			} catch (e) {
				app.error(`muuri.init`, e)
			}
 		},


		/* Function desc */
 		update(muuri = false) {
			try {
				if(!muuri)
					return

				muuri.synchronize()
				muuri.refreshItems()
				muuri.layout()

				$.each($(muuri._element).find('.order'), (i, request) => {
					muuri.move(request, $(request).data('position'))
				})

				/*
				if(app.is.mobile())
					$(muuri._element).closest('.flickity').flickity('resize')
				*/
				
				$('.days').flickity('resize')
			} catch(e) {
				app.error(`muuri.update`, e)
			}
		 }
 	}
 })()
