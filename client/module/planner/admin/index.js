import { app } from '../../app/'
const moment = require('moment')
 
export const planner = (() => {
	'use strict'
 
	return {
		async init() {
			try {
				const $planner = $('.plan') 
				const $map   = await app.map.init($planner.find('#map'))
				const $dates = $planner.find('.dates')
				const $days  = $planner.find('.days')
				const { pageDots, prevNextButtons } = false
				const cellAlign = app.is.mobile() ? 'center' : 'left'

				$planner.addClass('loading')
				await this.template.init()

				/* START of DATES AND DAYS create */
				for (let i = 30; i > -30; i--) {
					let day = moment().add(i, 'days').format('MM/DD')
					let weekday = moment().add(i, 'days').format('dddd')
					let name = moment().add(i, 'days').format('dddd')
					let dayname = moment().add(i, 'days').format('ddd')

					switch(i) { 
						case 1:
							name = 'Tomorrow'
							break

						case 0:
							name = 'Today'
							day = '<i data-feather="sun"></i>'
							break

						case -1:
							name = 'Yesterday'
							break
					}

					const $day = $(app.template('admin/me/day')({ name }))
						.data('date', moment().add(i, 'days').format('YYYY-MM-DD'))
						.addClass(moment().add(i, 'days').format('YYYY-MM-DD'))
						.data('handle', i >= 0 ? true : false) 

					const $date = $(i === 0 ? `<span style="width: 70px;">${day}</span>` : `<span>${day}<span class="dayname">${dayname}</span></span>`)
						.data('day', $day)
					
					if(weekday === 'Sunday' || weekday == 'Saturday')
						$date.addClass('weekend')

					$dates.prepend($date)
					$days.prepend($day)
				}

				await app.ui.update()
				/* END of DATES AND DAYS create */

				$dates.flickity({
					pageDots, prevNextButtons, cellAlign,
					asNavFor: $days[0],
					contain: true,
					freeScroll: true
				})

				$days.flickity({
					pageDots, prevNextButtons, cellAlign,
					draggable: false,
					adaptiveHeight: true,
					on: {
				    async change(index) {
						$.each($days.find('.day'), async (i, day) => {
							if(i === index) {
								WebuiPopovers.hideAll()
								const $day = $(day)
								const $masters = $day.find('.masters')

								/* UI INIT */
								$day.find('h3').animateCss('fadeIn')
								$masters.addClass('loading')
								/* UI INIT */

								/* DAY LOAD */
								await $days.flickity('resize')
								await app.map.direction.remove()

								await app.request.load('day', $masters[0], async e => {
									$masters.removeClass('loading')
									await $days.flickity('resize')
								})
								/* DAY LOAD */
							}
						})

						app.webkit.taptic()
						await app.sound.click()
				    }
				  }
				})


				await this.socket.connect()
				await $days.flickity('select', 29)
				await app.request.load('backlog', $('.backlog .orders')[0])
				$map.setZoom(9)

				setTimeout(() => $planner.removeClass('loading'), 1000)
				await $dates.flickity('resize')

				/* ACTION HANDLERS */
				$(document).on('click', '.action.ready', e => {
					e.originalEvent.preventDefault()
					const $master = $(e.currentTarget).closest('.master')

					$master.hasClass('ready') ? master.unlock($master) : master.lock($master)
				}) 

				setInterval(() => {
					$('.serverTime span').html(moment().local().format('LT'))
				}, 1000)
			} catch (e) {
				app.error(`planner.admin`, e)
			}
		}
 	}
})()

 
import { socket } from './module/socket'
planner.socket = socket

import { request } from './module/request/'
planner.request = request

import { master } from './module/master/'
planner.master = master

import { muuri } from './module/muuri'
planner.muuri = muuri

import { template } from './module/template'
planner.template = template
 
import { popover } from './module/popover/index'
planner.popover = popover 

import { vendor } from './module/vendor/'
planner.vendor = vendor
import { manager } from './module/manager/'
planner.manager = manager