import { app } from '../../../../app/'
 
export const vendor = (() => {
 	'use strict'

 	return {
		async create(vendor = false) {
			try {
				if(!vendor)
					return 

				await app.planner.popover.vendors.add(vendor)
			} catch(e) {
				app.error('master > create', e)
			}
		}, 

		async change(id = false, param = false, value = false, additional = {}) {
			try {
				const $param = $(document).find(`[data-vendor-${id}-${param}]`) || false
				$param.html(value)
			} catch(e) {
				app.error('master > change', e)
			}
		}, 

		async remove(vendor = false) {
			try { 
				if(!vendor)
					return  

				await app.planner.popover.vendors.remove(vendor)
			} catch(e) {
				app.error('master > create', e)
			}
		}
 	}
 })()