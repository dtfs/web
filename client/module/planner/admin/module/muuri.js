import { app } from '../../../app/'
import { planner } from '../'
  
const Muuri = require('muuri')

export const muuri = (() => {
 	'use strict'


 	return {
 		async init(block = false) {
			if(!block)
				 return

			const $map = $(document).find('#map').data('map')
			let dragStartPredicate = {
				distance: app.is.mobile() ? 0 : 1,
				delay: app.is.mobile() ? 300 : 0,
				handle: '.draggable'
			}
			let SCROLLING = false

 			try {
 				const muuri = new Muuri(block, {
 					items: '.drag',
 					dragEnabled: true,
 					dragContainer: $('main')[0],
					layoutOnResize: true, 
 					dragSort() {
						let columnGrids = []
						$.each($(document).find('.muuri'), (i,e) => columnGrids.push($(e).data('muuri')))
						return columnGrids
 					},
					dragStartPredicate,
					dragHammerSettings: {
						touchAction: 'pan-x'
					 },
					layout: { 
						horizontal: $(block).data('type') === 'backlog' ? app.is.mobile() : false 
					}
 				}).on('send', async item => {
 					const $item = $(item.item._element)
 					$item.data('fromGrid', $(item.fromGrid._element))
 					$item.data('toGrid', $(item.toGrid._element))
 				}).on('dragInit', async item => {
 					$('html').addClass('dragging')
 					const $item = $(item._element)
 					$item.data('fromGrid', $item.parent())
 					$item.data('toGrid', $item.parent())
					$item.find('.badge.history').button()
					$item.css({ 'width': $item.width(), 'height': $item.height() })

					await app.socket.send({ action: 'dragStart', request: $item.data('id') })
					await app.webkit.taptic()
					await app.sound.click()
 				}).on('dragReleaseEnd', async item => {
					$('html').removeClass('dragging')
					item.getElement().style.width = ''
					item.getElement().style.height = ''

					await this.update(muuri)

					const $item = $(item._element)
					const id = $item.data('id')
 					const $marker = $item.data('marker')
 					const $from = $item.data('fromGrid')
 					const $to = $item.data('toGrid')

					let master = null
					let date = null


					if($to.data('type') === 'backlog' && app.is.mobile())
						muuri.move($to.find('.desktop-remove')[0], 0)
					$item.find('.badge.history').button(false)

					if($to.data('type') !== 'backlog') {
						let i = 0
						for(const e of $to.find('.order')) {
							const $request = $(e)
							const position = parseInt($request.data('position')) || 0
	
							if(position !== parseInt(i)) {
								$request.data('position', i)
								$request.attr('data-position', i)
	
								await app.request.change($request.data('id'), {
									param: 'position',
									value: i
								})
								await app.socket.send({
									action: 'position',
									request: $request.data('id'),
									value: i
								})
							}
							++i
						}
					}
					 

					// In change position in one list
					if($from[0] === $to[0]) {
						$from.closest('.master').addClass('routed')
						await planner.master.update($from.closest('.master'))
						await app.socket.send({ action: 'dragEnd', request: id })
						$item.removeClass('disabled')
						
						this.update(muuri)
						app.map.direction.create($to)
						return
					}

					// If moved from one to another
					switch($from.data('type')) {
						case 'backlog': {
							master = $to.data('typeId')
							date = $to.closest('.day').data('date')

							$to.closest('.master').addClass('routed')
							await planner.master.update($to.closest('.master'))
							app.map.direction.create($to)
							break
						}

						case 'master': {
							switch($to.data('type')) {
								case 'backlog': {
									await app.map.marker.change($marker, {type: app.request.status(2)})
									$marker.setMap($map)

									master = null
									date = null

									await planner.master.update($from.closest('.master'))
									app.map.direction.remove()
									break
								}

								case 'master': {
									master = $to.data('typeId')
									date = $to.closest('.day').data('date')

									$to.closest('.master').addClass('routed')
									await planner.master.update($from.closest('.master'))
									await planner.master.update($to.closest('.master'))
									app.map.direction.create($to)
									break
								}
							}
							break
						}
					}

					await app.request.change(id, { param: 'date', value: date })
					await app.request.change(id, { param: 'master', value: master }, async e => {
						if($item.data('status') !== 'changed' && master !== null)
							await app.request.change(id, { param: 'status', value: app.request.status('changed') })
					})

					await app.socket.send({
						action: 'move',
						request: id,
						to: { master: master, date: date, position: $item.data('position') }
					})

					await app.socket.send({ action: 'dragEnd', request: id })
					this.update(muuri)
				 })
				 
				 $(block).closest('.orders').data('muuri', muuri)
 			} catch (e) {
				app.error(`muuri.init`, e)
			}
 		},


		/* Function desc */
 		async update(muuri = false) {
			try {
				if(!muuri)
					return

				
				if(app.is.mobile()) {
					const $backlog = $('.backlog')
					const $orders = $backlog.find('.orders')
					let width = 0

					$.each($orders.find('.order'), (i, order) => {
						const w = 350
						width += w
					})

					if(width > 0)
						$orders.width(width)
				}

				await muuri.refreshItems()
				await muuri.synchronize()
				await muuri.layout()

				await $('.days').flickity('resize')
			} catch(e) {
				app.error(`muuri.update`, e)
			}
		 },
		 
		 async reposition(muuri = false) {
			$.each($(muuri._element).find('.order'), (i, request) => {
				muuri.move(request, $(request).data('position'))
			})
		 }
 	}
 })()
