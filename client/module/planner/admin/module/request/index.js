import { app } from '../../../../app/'
  
export const request = (() => {
 	'use strict'

 	return {
		/* Function desc */
 		async add($block = $(document).find('.backlog .orders'), data = false) {
			try {
				if(!data || !$block)
					return

				data.distance = {
					text: `${Math.round(data.distance / 1609.344)} mi`,
					value: data.distance
				}
				data.status = app.request.status(parseInt(data.status))

				const $map = $('#map').data('map')
				const $request = $(app.template(`admin/me/order`)(data))
				$request.data(data)
				const $master = $block.closest('.master') || false

				if($(`.order__${data.id}`).length > 0)
					$.each($(`.order__${data.id}`), (i,e) => this.remove($(e)))

				if($($block).data('muuri'))
					$($block).data('muuri').add([$request[0]], {index: parseInt(data.position) + 1 || 1})
				else
					$($block).append($request)
				
				const $marker = await app.map.marker.create($map, {
					id: data.id,
					type: data.status,
					position: new google.maps.LatLng(data.lat, data.lng)
				})
				$request.data('marker', $marker)

				$marker.setMap(null)
				if($master.length === 0 || !$master)
					$marker.setMap($map)
				else {
					$request.data('master', $master.data('data').id)
					$request.data('date', $master.closest('.day').data('.date'))
				}

				await this.ui($request, data)
				await app.planner.master.ui.update($master)

				const muuri = $request.closest('.muuri').data('muuri') || false
				await app.planner.muuri.update(muuri)
				await app.planner.muuri.reposition(muuri)
				setTimeout(() => app.planner.muuri.update(muuri), 300)


				if($($block).data('type') === 'backlog' && app.is.mobile())
					muuri.move($($block).find('.desktop-remove')[0], 0)
			} catch(e) {
				app.error('request.add', e)
			}
 		},


		/* Function REMOVE */
 		async remove($request) {
			try {
				const $requests = $request.closest('.orders')
				const $marker = $request.data('marker') || false
				const $master = $request.closest('.master') || false
				const muuri = $request.closest('.muuri').data('muuri') || false

				$request.remove()

				if($marker)
					$marker.setMap(null)

				await app.planner.master.update($requests.closest('.master'))
				
				await app.planner.muuri.update(muuri)
				await app.planner.muuri.reposition(muuri)
			} catch(e) {
				app.error('request.remove', e)
			}
 		},


		/* Function desc */
		async ui($request, data) {
			try {
				const $badge = $request.find('.history.badge')
				$badge.requestHistory()
			} catch(e) {
				app.error('request.remove', e)
			}
		}
 	}
 })()
