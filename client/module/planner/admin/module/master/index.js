import { app } from '../../../../app/'
const feather = require('feather-icons')
const moment = require('moment')
 
export const master = (() => {
 	'use strict'

 	return {
		async create(master = false) {
			try {
				if(!master)
					return 

				await app.planner.popover.masters.add(master)
				await app.planner.master.add(master)
			} catch(e) {
				app.error('master > create', e)
			}
		}, 

		async change(id = false, param = false, value = false, additional = {}) {
			try {
				const $param = $(document).find(`[data-master-${id}-${param}]`) || false
				
				switch(param) {
					case 'address':
						const marker = $(document).find(`[data-master-${id}]`).data('marker')
						await app.map.marker.position( marker, additional.lat, additional.lng )
						break
				}

				$param.html(value)
			} catch(e) {
				app.error('master > change', e)
			}
		},

 		async add(data = {}, $block = $(document).find('.day.is-selected .masters'), callback) {
			try {
				data.avatar = data.id % 26

				if(data.vacation)
					for(const vacation of data.vacation) {
						const date =  moment($($block).closest('.day').data('date')).toDate().setHours(0)
						const from = moment(vacation.from).toDate().setHours(0)
						const to = moment(vacation.to).toDate().setHours(0)

						if(from <= date && to >= date)
							data.onvacation = true
					}
				
				const $map = $('#map').data('map')
				const $master = $(app.template(`admin/me/master`)(data))
				$master.data('data', data)
				$block.append($master[0])

				if(!data.onvacation) {
					$master.data('marker', app.map.marker.create($map, {
						id: data.id,
						type: 'master',
						master: $master,
						position: new google.maps.LatLng(data.lat, data.lng)
					}))
				}

				let ready = data.request ? true : false
				
				if ($master.closest('.day').data('handle') !== true)
					$master.find('.action.ready').remove()

				if (ready) {
					$master.find('.action.ready').addClass('active').html(feather.icons.lock.toSvg())
					$master.addClass('ready')
				}

				if($master.closest('.day').data('handle'))
					await app.planner.muuri.init($master.find('.orders')[0])

				/* Add new request to master list */
				$.each(data.request, async (i, item) => {
					if ($($block).closest('.day').data('handle') === true) {
						item.drag = 'drag'

						if (item.status < 2) {
							ready = false
							item.draggable = 'draggable'
						}
					}

					await app.planner.request.add($master.find('.orders'), item)
				})

				await master.update($master)
			} catch(e) {
				app.error('master > add', e)
			}
		 },

		async remove(master = false) {
			try { 
				if(!master) return  
				await app.planner.popover.masters.remove(master)
			} catch(e) {
				app.error('master > create', e)
			}
		},


 		async update($master = false) {
			try {
				if(!$master)
					return

				await master.ui.update($master)
				await app.planner.muuri.update($master.find('.muuri').data('muuri'))
			} catch(e) {
				app.error('master.update', e)
			}
 		},


		async lock($master) {
			try {
				$.each($master.find('.order'), async (i,e) => {
					const $request = $(e)

					if (
						$request.data('status') === 'changed'
						|| $request.data('status') === 'new'
					)
						await app.request.change($request.data('id'), {
							param: 'status',
							value: app.request.status('waiting')
						})
				})

				let ready = true
				$.each($master.find('.order'), (i,e) => {
					if(parseInt(app.request.status($(e).data('status'))) < 2)
						ready = false
				})

				if(ready)
					return 

				master.ui.lock($master)
				await app.planner.muuri.update($master.find('.muuri').data('muuri'))
				app.sound.lock()
				
				const data = $master.data('data')
				await app.post('request/invoice', 'miles', {
					master: data.id,
					miles: $master.data('miles'),
					date: $master.closest('.day').data('date'),
				})
			} catch (e) { 
				app.error(`admin.master.lock`, e)
			}
		},


		async unlock($master) {
			try {
				$.each($master.find('.order'), async (i,e) => {
					const $request = $(e)

					if ($request.data('status') === 'waiting')
						await app.request.change($request.data('id'), {
								param: 'status',
								value: app.request.status('changed')
						})
				})

				let ready = true
				$.each($master.find('.order'), (i,e) => {
					if(parseInt(app.request.status($(e).data('status'))) <= 2)
						ready = false
				})

				if(ready)
					return 

				master.ui.unlock($master)
				await app.planner.muuri.update($master.find('.muuri').data('muuri'))
				app.sound.lock()
			} catch (e) {
				app.error(`admin.master.unlock`, e)
			}
		}
 	}
 })()





import { ui } from './module/ui'
master.ui = ui 

import { vacation } from './module/vacation'
master.vacation = vacation

import { invoice } from './module/invoice'
master.invoice = invoice