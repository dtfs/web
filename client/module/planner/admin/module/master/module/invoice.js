import { app } from '../../../../../app/'
import moment from 'moment'

export const invoice = (() => {
	'use strict'

	return {
		async create(invoice = false, callback) {
			try {
				await app.post('user/invoice', 'create', invoice, async data => {
					if(!data) {
						if(callback)
							callback(data)
						return
					}


					if(callback)
						callback(data)
				})
			} catch(e) {
				app.error('admin.master.invoice.create')
			}
		}, 

		async add(invoice = false, $block, prepend = false, callback) {
			try {
				switch(parseInt(invoice.status)) {
					case 1: {
						invoice.status = 'invoiced'
						break
					}
					
					case 2: {
						invoice.status = 'payed'
						break
					}

					case 3: {
						invoice.status = 'canceled'
						break
					}

					case 4: {
						invoice.status = 'exported'
						break
					}
				}
				
				if(invoice.request)
					invoice.request.status = app.request.status(parseInt(invoice.request.status))
				if(invoice.report)
					invoice.report = JSON.parse(invoice.report)
				invoice.date = flatpickr.formatDate(flatpickr.parseDate(invoice.date, 'Y-m-d'), "M d, Y")
				invoice.price = invoice.price / 100
					
				const $invoice = $(app.template('admin/popover/master/invoice')(invoice))
				prepend ? $block.prepend($invoice) : $block.append($invoice)

				if(invoice.report) {
					const $report = $(app.template(`request/report/furniture`)(invoice))
					$invoice.find('ul[data-report]').html($report)

					$report.on('change', async e => {
						e.originalEvent.preventDefault()
						const $this = $(e.currentTarget)
						await app.request.report.change(invoice.request.id, $this.serializeArray())
					}).on('submit', e => {
						e.originalEvent.preventDefault()
						const $this = $(e.currentTarget)
						$this.closest('ul.active').find('a.back').trigger('click')
					})
				}
				 
				$invoice.find('.pay').on('click', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					$this.button()
					await app.post('request/invoice', 'pay', {
						request: invoice.request.id,
						id: invoice.id
					}, async data => {
						await app.socket.send({
							action: 'invoice/change', 
							master: invoice.request.master,
							invoice: invoice.id,
							status: 'payed'
						})

						app.request.report.status(invoice.id, 'payed')

						setTimeout(() => {
							$this.button(false)
							$this.closest('ul.active').find('a.back').trigger('click')
						}, 300)
					})

					
				})

				$invoice.find('.cancel').on('click', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					$this.button()
					await app.post('request/invoice', 'cancel', {
						request: invoice.request.id,
						id: invoice.id
					}, async data => {
						await app.socket.send({
							action: 'invoice/change', 
							master: invoice.request.master,
							invoice: invoice.id,
							status: 'canceled'
						})
						app.request.report.status(invoice.id, 'canceled')

						setTimeout(() => {
							$this.button(false)
							$this.closest('ul.active').find('a.back').trigger('click')
						}, 300)
					})
				})


				$invoice.find('.export').on('click', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					$this.button()
					app.debug('test', invoice)
					await app.pdf.createInvoice(invoice)
					await app.pdf.save(`Invoice. ${invoice.master.fname} ${invoice.master.lname}. ${moment(new Date(invoice.date)).format('MMM D, YYYY')}`) 
					$this.button(false)
					$this.closest('ul.active').find('a.back').trigger('click')
				})

				app.ui.update()
				if(callback) 
					await callback(invoice)
			} catch(e) {
				app.error('admin.master.invoice.add', e)
			}
		},

		async remove(id = false, callback) {
			try {
				if(!id) return false

				await app.post('user/invoice', 'remove', { id }, async data => {
					if(!data) {
						if(callback) 
							callback(false)

						return
					}

					if(callback) 
						callback(data)
				})
			} catch(e) {
				app.error('admin.master.invoice.create')
			}
		}
	}
})()