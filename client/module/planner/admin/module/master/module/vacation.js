import {
	app
} from '../../../../../app/'
const moment = require('moment')

export const vacation = (() => {
	'use strict'

	return {
		async create(vacation = false, callback) {
			try {
				await app.post('user/vacation', 'create', vacation, async vacation => {
					if (!vacation)
						return

					await app.socket.send({
						action: 'master/vacation/create',
						master: vacation.master,
						vacation
					})
					await app.webkit.taptic()

					if (callback)
						callback(vacation)
				})
			} catch (e) {
				app.error('admin > master > vacation > create')
			}
		},

		async add(vacation = false, $block = false, prepend = false, callback) {
			try {
				$block = $block ? $block : $(document).find(`.list.masters .master-${vacation.master} .list.vacations`)
				const from = moment(vacation.from).toDate().setHours(0)
				const to = moment(vacation.to).toDate().setHours(0)
				const date = moment($('.day.is-selected').data('date')).toDate().setHours(0)

				if (from <= date && to >= date)
					await $(`.master__${vacation.master}`).addClass('vacation')

				vacation.from = flatpickr.formatDate(flatpickr.parseDate(vacation.from, 'Y-m-d'), "M d, Y")
				vacation.to = flatpickr.formatDate(flatpickr.parseDate(vacation.to, 'Y-m-d'), "M d, Y")

				const $vacation = await $(app.template('admin/popover/master_vacation')(vacation))
				prepend ? $block.prepend($vacation) : $block.append($vacation)

				$vacation.find('.remove').on('click', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					await $this.button()
					await app.webkit.taptic()
					await app.post('user/vacation', 'remove', {
						id: vacation.id
					}, async data => {
						if (!data)
							return

						await this.remove(vacation, $vacation)
						await app.socket.send({
							action: 'master/vacation/remove',
							master: vacation.master,
							vacation
						})
						await app.webkit.taptic()
					})
					await $this.button(false)
				})

				if (callback)
					await callback(vacation)

				await app.ui.update()
			} catch (e) {
				app.error('admin > master > vacation > add', e)
			}
		},

		async remove(vacation = false, $vacation = $(document).find(`.vacation__${vacation.id}`)) {
			try {
				if (!vacation)
					return

				$vacation.remove()

				const date = moment($('.day.is-selected').data('date')).toDate().setHours(0)
				const from = moment(vacation.from).toDate()
				const to = moment(vacation.to).toDate()

				if (from <= date && to >= date)
					$(`.master__${vacation.master}`).removeClass('vacation')
			} catch (e) {
				app.error('admin > master > vacation > remove', e)
			}
		}
	}
})()