import { app } from '../../../../../app/'
const feather = require('feather-icons')
 
export const ui = (() => {
	'use strict'

	return {
		async lock($master) {
			if(!$master)
				return

			$master.addClass('ready')
				.find('.action.ready')
				.addClass('active')
				.html(feather.icons.lock.toSvg())

			//await app.planner.muuri.update($master.find('.orders').data('muuri'))
		},

		async unlock($master) {
			if(!$master)
				return

			$master.removeClass('ready')
				.find('.action.ready')
				.removeClass('active')
				.html(feather.icons.unlock.toSvg())

			//await app.planner.muuri.update($master.find('.orders').data('muuri'))
		},

		async direction($master) {
			if(!$master)
				return

			try {
				if($master.hasClass('routed')) {
					app.map.direction.create($master)
				}
			} catch(e) {
				app.error('master.ui.direction', e)
			}
		},

		async update($master) {
			if(!$master)
				return

			if($master.find('.order').length > 0)
				$master.find('.action.ready').show()
			else
				$master.find('.action.ready').hide()

			let ready = true
			$.each($master.find('.order'), (i,e) => {
				if(parseInt(app.request.status($(e).data('status'))) < 2)
					ready = false
			}) 

			ready ? app.planner.master.ui.lock($master) : app.planner.master.ui.unlock($master)
			await $('.days').flickity('resize')
		}
	}
})()