import { templateLoader } from '../../../app/module/template'
import { app } from '../../../app/'
  
export const template = (() => {
   'use strict'

   return {
      async init() {
         try {
            /* MASTERS */
            await templateLoader.register({
               key: `admin/popover/masters`,
               path: `/client/template/admin/popover/masters/index.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/master`,
               path: `/client/template/admin/popover/masters/module/master.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/master_create`,
               path: `/client/template/admin/popover/masters/module/create.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/master_vacation`,
               path: `/client/template/admin/popover/masters/module/vacation.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/master/invoice`,
               path: `/client/template/admin/popover/masters/module/invoice.html?v=${app.VERSION}`
            })
            /* MASTERS */


            /* VENDORS */ 
            await templateLoader.register({
               key: `admin/popover/vendors`,
               path: `/client/template/admin/popover/vendors/index.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/vendor`,
               path: `/client/template/admin/popover/vendors/module/vendor.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/vendor_create`,
               path: `/client/template/admin/popover/vendors/module/create.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/vendor/invoice`,
               path: `/client/template/admin/popover/vendors/module/invoice.html?v=${app.VERSION}`
            })
            /* VENDORS */


            /* MANAGERS */
            await templateLoader.register({
               key: `admin/popover/managers`,
               path: `/client/template/admin/popover/managers/index.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/manager`,
               path: `/client/template/admin/popover/managers/module/manager.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/popover/manager_create`,
               path: `/client/template/admin/popover/managers/module/create.html?v=${app.VERSION}`
            })
            /* MANAGERS */


            /* PLANNER */
            await templateLoader.register({
               key: `admin/me/day`,
               path: `/client/template/admin/day/index.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/me/master`,
               path: `/client/template/admin/master/index.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/me/order`,
               path: `/client/template/admin/request/index.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: `admin/me/order__menu`,
               path: `/client/template/admin/request/module/menu.html?v=${app.VERSION}`
            })
            await templateLoader.register({
               key: 'admin/create/request',
               path: `/client/template/admin/popover/request/create.html?v=${app.VERSION}`
            })
            /* PLANNER */
         } catch(e) {

         }
      }
   }
})()