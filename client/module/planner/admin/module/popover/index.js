import { app } from '../../../../app/'
 
export const popover = (() => {
   'use strict'
 
   return {
      menu() {
         try {

         } catch(e) {
            app.error('popover > menu', e)
         }
      },
 
      async map($this, $content) {
         try { 
            await app.map.init($content.find('.map-ui'), async map => {
               const $master = $this.closest('.master')
               const master = $master.data('data')
               const marker = $master.data('marker') || false
               
               if(marker) {
                  await marker.setMap(map)
                  await map.setCenter(marker.position)
               }

               app.map.direction.create($this, false, map, response => {
                  const route = []
                  $.each($master.find('.order'), (i, order) => {
                     route.push($(order).data('address'))
                  })

                  const origin = master.address.replace(/ /g, '+')
                  let waypoints = ''
                  $.each(route, (i, address) => {
                     address = address.replace(/ /g, '+')
                     waypoints = `${waypoints}${address}`
                     waypoints = i === route.length - 1 ? `${waypoints}` : `${waypoints}/`
                  })

                  const link = `https://www.google.com/maps/dir/${origin}/${waypoints}/${origin}`
                  $content.find('.map--button').append(`
                     <a href="${link}" class="btn button large w-100" target="_blank">
                        Open in Google Maps
                     </a>
                  `)
               })
            })
         } catch(e) {
            app.error('popover > map', e)
         }
      }
   }
})()

 
import { masters } from './module/masters'
popover.masters = masters
import { vendors } from './module/vendors'
popover.vendors = vendors 
import { managers } from './module/managers'
popover.managers = managers 