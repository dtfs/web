import { app } from '../../../../../../app'
import moment from 'moment'

export const vendors = (() => {
   'use strict'
     
   return {
      async init($popover) { 
         try {
            this.$this = $(app.template(`admin/popover/vendors`)())
            await app.post('user', 'get', { type: 'vendor' }, e => this.vendor = e)
            
            for(const vendor of this.vendor)
               await this.add(vendor)

            /* Create form */
            const $createForm = $(app.template('admin/popover/vendor_create')())
            $createForm.insertAfter(this.$this.find('.vendors'))
            $createForm.find('form').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const form = $form.serializeArray()
               const $button = $form.find('button')
               const vendor = { type: 3 }

               for(const e of form)
                  vendor[e.name] = e.value

               $button.button()
               await app.post('user', 'create', vendor, id => vendor.id = id)
               await app.socket.send({ action: 'vendor/create', vendor })
               this.create(vendor)
               $button.prop('disabled', true).button(false)
               $form.closest('.slinky').data('slinky').home()
            })
            /* Create form */
            
            await $popover.html(this.$this)
            await app.ui.update()
         } catch(e) {
            app.error('popover.vendor > init', e)
         }
      },
      
      async create(vendor = {}) {
         try {
            await app.planner.vendor.create(vendor)
         } catch(e) {
            app.error('popover > vendor', e)
         }
      },

      async add(vendor = false) {
         try {
            if(!vendor || !this.$this) 
               return 

            vendor.avatar = vendor.id % 26
            if(!vendor.invoice)
               vendor.invoice = []
            if(!vendor.document)
               vendor.document = []
            if(!vendor.pricing)
               vendor.pricing = []

            const $vendor = $(app.template('admin/popover/vendor')(vendor))
            vendor.$vendor = $vendor
            this.$this.find('.vendors').append($vendor)
            
            $vendor.find('form.change').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const $button = $form.find('button')
               const data = []

               for(const input of $form.find('input.changed, select.changed, textarea.changed')) {
                  const name = $(input).attr('name')
                  const value = name === 'phone' ? `1${$(input).cleanVal()}` : $(input).val()
                  data[name] = value

                  $(input).removeClass('changed')
               }
               
               for(const param in data) {
                  $button.button()

                  const additional = param === 'address' ? { lat: data.lat, lng: data.lng } : false
                  const value = data[param]

                  app.debug(vendor.id, {param, value})
                  await app.user.change(vendor.id, { param, value }, async response => {
                     $button.button(false) 

                     await app.planner.vendor.change(vendor.id, param, value, additional)
                     await app.socket.send({ 
                        action: `vendor/change`, 
                        vendor: vendor.id, 
                        param, value, additional
                     })
                     await this.$this.data('slinky').jump(`ul.vendor-${vendor.id}`)
                  })
               }	
            })

            $vendor.find('form.remove').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const $button = $form.find('button')
               const id = $form.data('id')

               $button.button()
               await app.post('user', 'remove', { id })
               await app.socket.send({ 
                  action: `vendor/remove`, 
                  vendor: id
               })

               $form.closest('.slinky').data('slinky').home()
               app.planner.vendor.remove(id)
               $button.button(false)
            })
            

            /* INVOICE UI */
            const $invoiceLoad = $vendor.find('.load-invoice')
            $invoiceLoad.on('mousedown', async e => {
               const $this = $(e.currentTarget)
               const $invoices = $this.closest('li').find('.list')
               $invoices.html('')
                  .addClass('loading')

               setTimeout(async () => { 
                  await app.post('user/invoice', 'get', { vendor: vendor.id }, async data => vendor.invoice = data)

                  for(const invoice of vendor.invoice)
                     app.planner.popover.vendors.invoice.add(invoice, $vendor.find('.list.invoices'))

                  $invoices.removeClass('loading')
                  $invoiceLoad.closest('.slinky.main').data('slinky').jump('ul.active')
               }, 300)
            })

				const $invoiceExportDate = $vendor.find('form.exportDate')
				const $invoiceExportDateButton = $invoiceExportDate.find('button')
				const $invoiceExportDateInput = $invoiceExportDate.find('input.invoice_dates')
				$invoiceExportDateInput.on('click touchstart', e => {
				   flatpickr(e.currentTarget, {
					  mode: 'range',
					  dateFormat: 'M d, Y',
					  onChange(selectedDates, dateStr, instance) {
						 if(selectedDates.length === 2) { 
							const [ from, to ] = [
							   instance.formatDate(selectedDates[0], 'Y-m-d'),
							   instance.formatDate(selectedDates[1], 'Y-m-d')
							]
							 
							$invoiceExportDate.find('input[name="from"]').val(from)
							$invoiceExportDate.find('input[name="to"]').val(to)
						 }
	
						 $invoiceExportDateButton.prop('disabled', selectedDates.length === 2 ? false : true)
						 app.webkit.taptic()
					  },
					  onOpen(selectedDates, dateStr, instance) {
						 $(instance.element).data('flatpickr', instance)
						 $(instance.calendarContainer).removeClass('animate')
						 app.webkit.taptic()
					  }
				   }).open()
            })
            
            $invoiceExportDate.on('click', 'button.exportDate', async e => {
               e.originalEvent.preventDefault()
               const $button = $(e.currentTarget)
               const $form = $button.closest('form')
               const type = $button.data('type')
               const from = $form.find('input[name="from"]').val()
               const to = $form.find('input[name="to"]').val()

               
               $button.button()
               await app.post('request/invoice', 'get', {
                  from, to, vendor: vendor.id
               }, async invoices => {
                  if(!invoices || !invoices.length) {
                     $button.button(false)
                     return
                  }

                  switch(type) {
                     case 'invoice': {
                        await app.pdf.createTotal(invoices, from, to)
                        await app.pdf.save(`Invoice. ${vendor.company_name}. ${moment(new Date(from)).format('MMM D, YYYY')} - ${moment(new Date(to)).format('MMM D, YYYY')}`) 
                        break
                     }

                     case 'report': {
                        for(const invoice of invoices)
                           await app.pdf.createInvoice(invoice)
                        await app.pdf.save(`Report. ${vendor.company_name}. ${moment(new Date(from)).format('MMM D, YYYY')} - ${moment(new Date(to)).format('MMM D, YYYY')}`) 
                        break
                     }
                  } 

                  for(const invoice of invoices) {
                     if(invoice.status !== 4 && invoice.status !== 'exported') {
                        $(document).find(`[data-invoice-handler="${invoice.id}"]`).attr('data-status', 'exported')
                        .data('status', 'exported')

                        await app.socket.send({
                           action: 'invoice/change', 
                           invoice: invoice.id,
                           status: 'exported'
                        })
                     }
                  }
                     
                  $button.button(false)
               })
            })
            /* INVOICE UI */

            await this.rebuild()
         } catch(e) {
            app.error('popover.vendor > add', e)
         }
      },

      async remove(vendor = false) {
         try {
            if($(`ul.vendor-${vendor}.active`).length > 0)
               this.$this.data('slinky').home()
               
            $(document).find(`[data-vendor-handler="${vendor}"]`).remove()
         } catch(e) {
            app.error('popover.vendor > remove', e)
         }
      },

      async rebuild() {
         try {
            const slinky = this.$this.data('slinky') || false

            if(!slinky)
               return

            await slinky.rebuild()  
            await app.ui.update()
         } catch(e) {
            app.error('popover.vendor > rebuild', e)
         }
      }
   }
})()

import { invoice } from './module/invoice'
vendors.invoice = invoice