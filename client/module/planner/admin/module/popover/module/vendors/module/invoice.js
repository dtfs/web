import { app } from '../../../../../../../app'
import moment from 'moment'

export const invoice = (() => {
	'use strict'

	return {
		async add(invoice = false, $block, prepend = false, callback) {
			try {
				switch(parseInt(invoice.status)) {
					case 1: {
						invoice.status = 'invoiced'
						break
					}
					
					case 2: {
						invoice.status = 'payed'
						break
					}

					case 3: {
						invoice.status = 'canceled'
						break
					}

					case 4: {
						invoice.status = 'exported'
						break
					}
				}

				if(invoice.request)
					invoice.request.status = app.request.status(parseInt(invoice.request.status))
				if(invoice.report)
					invoice.report = JSON.parse(invoice.report)
				invoice.date = flatpickr.formatDate(flatpickr.parseDate(invoice.date, 'Y-m-d'), "M d, Y")
				invoice.price = invoice.price / 100

				const $invoice = $(app.template('admin/popover/vendor/invoice')(invoice))
				prepend ? $block.prepend($invoice) : $block.append($invoice)

				
				if(invoice.report) {
					const $report = $(app.template(`request/report/furniture`)(invoice))
					$invoice.find('ul[data-report]').html($report)
				}


				$invoice.find('.export').on('click', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					$this.button()
					await app.pdf.createInvoice(invoice)
					await app.pdf.save(`Invoice. ${invoice.request.vendor.company_name}. ${moment(new Date(invoice.date)).format('MMM D, YYYY')}`) 
					$this.button(false)
					$this.closest('ul.active').find('a.back').trigger('click')
				})

				$invoice.find('.export').on('click', e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					$this.button()
					setTimeout(() => {
						$this.button(false)
						$this.closest('ul.active').find('a.back').trigger('click')
					}, 3000)
				})

				app.ui.update()
				if(callback)
					await callback(data)
			} catch(e) {
				app.error('admin.master.invoice.add', e)
			}
		}
	}
})()