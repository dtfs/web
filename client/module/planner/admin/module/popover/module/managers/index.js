import { app } from '../../../../../../app'
 
export const managers = (() => {
   'use strict'
     
   return {
      async init($popover) { 
         try { 
            this.$this = $(app.template(`admin/popover/managers`)())
            await app.post('user', 'get', { type: 'manager' }, e => this.manager = e)
             
            for(const manager of this.manager)
               await this.add(manager)

            /* Create form */
            const $createForm = $(app.template('admin/popover/manager_create')())
            $createForm.insertAfter(this.$this.find('.managers'))
            $createForm.find('form').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const form = $form.serializeArray()
               const phone = `1${$form.find('input[name="phone"]').cleanVal()}`
               const $button = $form.find('button')
               const manager = { type: 1 }

               for(const e of form)
                  manager[e.name] = e.value
               manager.phone = phone

               $button.button()
               await app.post('user', 'create', manager, id => manager.id = id)
               await app.socket.send({ action: 'manager/create', manager })
               await this.create(manager)
               $button.prop('disabled', true).button(false)
               $form.closest('.slinky').data('slinky').home()
            })
            /* Create form */
            
            await $popover.html(this.$this)
            await app.ui.update()
         } catch(e) {
            app.error('popover.manager > init', e)
         } 
      },
      
      async create(manager = {}) {
         try {
            await app.planner.manager.create(manager)
         } catch(e) {
            app.error('popover > manager', e)
         }
      },

      async add(manager = false) {
         try {
            if(!manager || !this.$this) 
               return 

            manager.avatar = manager.id % 26
            if(!manager.invoice)
               manager.invoice = []
            if(!manager.document)
               manager.document = []
            if(!manager.pricing)
               manager.pricing = []

            const $manager = $(app.template('admin/popover/manager')(manager))
            manager.$manager = $manager
            this.$this.find('.managers').append($manager)
            
            $manager.find('form.change').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const $button = $form.find('button')
               const data = []

               for(const input of $form.find('input.changed, select.changed, textarea.changed')) {
                  const name = $(input).attr('name')
                  const value = name === 'phone' ? `1${$(input).cleanVal()}` : $(input).val()
                  data[name] = value

                  $(input).removeClass('changed')
               }
               
               for(const param in data) {
                  $button.button()

                  const additional = param === 'address' ? { lat: data.lat, lng: data.lng } : false
                  const value = data[param]

                  await app.manager.change(manager.id, { param, value }, async response => {
                     $button.button(false) 

                     await app.planner.manager.change(manager.id, param, value, additional)
                     await app.socket.send({ 
                        action: `manager/change`, 
                        manager: manager.id, 
                        param, value, additional
                     })
                     await this.$this.data('slinky').jump(`ul.manager-${manager.id}`)
                  })
               }	
            })
  
            $manager.find('form.remove').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const $button = $form.find('button')
               const id = $form.data('id')

               $button.button()
               await app.post('user', 'remove', { id })
               await app.socket.send({ 
                  action: `manager/remove`, 
                  manager: id
               })

               $form.closest('.slinky').data('slinky').home()
               app.planner.manager.remove(id)
               $button.button(false)
            })
            

            /* INVOICE UI */
            const $invoice =  $manager.find('form[name="invoice"]')
            const $invoiceLoad = $manager.find('.load-invoice')
            const $invoice_button = $invoice.find('button')

            $invoiceLoad.on('mousedown', async e => {
               const $this = $(e.currentTarget)
               const $invoices = $this.closest('li').find('.list')
               $invoices.html('')
                  .addClass('loading')

               setTimeout(async () => {
                  await app.post('manager/invoice', 'get', { manager: manager.id }, async data => manager.invoice = data)
                  
                  for(const invoice of manager.invoice) {
                     invoice.date = flatpickr.formatDate(flatpickr.parseDate(invoice.date, 'Y-m-d'), "M d, Y")
                     invoice.money = (parseInt(invoice.service) + parseInt(invoice.miles)) / 100
                     app.planner.manager.invoice.add(invoice, $manager.find('.list.invoices'))
                  }

                  $invoices.removeClass('loading')
                  $invoiceLoad.closest('.slinky.main').data('slinky').jump('ul.active')
               }, 300)
               
            })


            $invoice.on('submit', async e => {
               e.originalEvent.preventDefault()
               const $this = $(e.currentTarget) 
               const invoice = {}

               invoice['manager'] = manager.id
               $.each($this.serializeArray(), (i, e) => {
                  invoice[e.name] = e.value
               })

               $invoice_button.button()
               await app.planner.manager.invoice.create(invoice, async data => {
                  if(!data) {
                     $invoice_button.button(false)
                     return
                  }

                  await app.planner.manager.invoice.add({
                     money: invoice.money,
                     date: flatpickr.formatDate(flatpickr.parseDate(invoice.date, 'Y-m-d'), "M d, Y"),
                     comment: invoice.comment,
                     id: data[0].id
                  }, $manager.find('.list.invoices'), true)

                  await this.$this.data('slinky').jump(`.manager-${manager.id} ul.vacation-block`)
                  await $invoice_button.button(false)
                  await app.ui.update()
               })
            })
            /* INVOICE UI */

            this.rebuild()
         } catch(e) {
            app.error('popover.manager > add', e)
         }
      },

      async remove(manager = false) {
         try {
            if($(`ul.manager-${manager}.active`).length > 0)
               this.$this.data('slinky').home()
               
            $(document).find(`[data-manager-handler="${manager}"]`).remove()
         } catch(e) {
            app.error('popover.manager > add', e)
         }
      },

      async rebuild() {
         try {
            const slinky = this.$this.data('slinky') || false

            if(!slinky)
               return

            await slinky.rebuild()  
            await app.ui.update()
         } catch(e) {
            app.error('popover.manager > rebuild', e)
         }
      }
   }
})()

