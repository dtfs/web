import { app } from '../../../../../../app' 
import moment from 'moment'

export const masters = (() => {
   'use strict'
    
   return {
      async init($popover) { 
         try {
            this.$this = $(app.template(`admin/popover/masters`)())
            await app.post('user', 'get', { type: 'master' }, e => this.masters = e)
            
            for(const master of this.masters)
               await this.add(master)

            /* Create form */ 
            const $createForm = $(app.template('admin/popover/master_create')())
            $createForm.insertAfter(this.$this.find('.masters'))
            $createForm.find('form').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const form = $form.serializeArray()
               const phone = `1${$form.find('input[name="phone"]').cleanVal()}`
               const $button = $form.find('button')
               const master = { type: 2 }

               for(const e of form)
                  master[e.name] = e.value
               master.phone = phone

               $button.button()
               await app.post('user', 'create', master, id => master.id = id)
               await app.socket.send({ action: 'master/create', master })
               await this.create(master)
               $button.prop('disabled', true).button(false)
               $form.closest('.slinky').data('slinky').home()
            })
            /* Create form */
            
            await $popover.html(this.$this)
            await app.ui.update()
         } catch(e) {
            app.error('popover.masters > init', e)
         }
      },
      
      async create(master = {}) {
         try {
            await app.planner.master.create(master)
         } catch(e) {
            app.error('popover > master', e)
         }
      },

      async add(master = false) {
         try {
            if(!master || !this.$this) 
               return 

            master.avatar = master.id % 26
            if(!master.vacation)
               master.vacation = []
            if(!master.invoice)
               master.invoice = []
            if(!master.document)
               master.document = []
            if(!master.pricing)
               master.pricing = []

            const $master = $(app.template('admin/popover/master')(master))
            master.$master = $master
            this.$this.find('.masters').append($master)
            
            $master.find('form.change').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const $button = $form.find('button')
               const data = []

               for(const input of $form.find('input.changed, select.changed, textarea.changed')) {
                  const name = $(input).attr('name')
                  const value = name === 'phone' ? `1${$(input).cleanVal()}` : $(input).val()
                  data[name] = value

                  $(input).removeClass('changed')
               }
               
               for(const param in data) {
                  $button.button()

                  const additional = param === 'address' ? { lat: data.lat, lng: data.lng } : false
                  const value = data[param]

                  await app.user.change(master.id, { param, value }, async response => {
                     $button.button(false) 

                     await app.planner.master.change(master.id, param, value, additional)
                     if(param === 'address') {
                        const marker = $(document).find(`[data-master-${master.id}]`).data('marker')
                        marker.map.setCenter(new google.maps.LatLng(additional.lat, additional.lng))
                     }
                     await app.socket.send({ 
                        action: `master/change`, 
                        master: master.id, 
                        param, value, additional
                     })
                     await this.$this.data('slinky').jump(`ul.master-${master.id}`)
                  })
               }	
            })


            $master.find('form.remove').on('submit', async e => {
               e.originalEvent.preventDefault()
               const $form = $(e.currentTarget)
               const $button = $form.find('button')
               const id = $form.data('id')

               $button.button()
               await app.post('user', 'remove', { id })
               await app.socket.send({ 
                  action: `master/remove`, 
                  manager: id
               })

               $form.closest('.slinky').data('slinky').home()
               app.planner.master.remove(id)
               $button.button(false)
            })
            

            /* INVOICE UI */
            const $invoice =  $master.find('form[name="invoice"]')
            const $invoiceLoad = $master.find('.load-invoice')
            const $invoice_button = $invoice.find('button')

            $invoiceLoad.on('click', async e => {
               const $this = $(e.currentTarget)
               const $invoices = $this.closest('li').find('.list')
               $invoices.html('')
                  .addClass('loading')

               setTimeout(async () => {
                  await app.post('user/invoice', 'get', { master: master.id }, async data => master.invoice = data)
                  
                  for(const invoice of master.invoice)
                     app.planner.master.invoice.add(invoice, $master.find('.list.invoices'))

                  $invoices.removeClass('loading')
                  $invoiceLoad.closest('.slinky.main').data('slinky').jump('ul.active')
               }, 300)
            })

            const $invoiceExportDate = $master.find('form.exportDate')
				const $invoiceExportDateButton = $invoiceExportDate.find('button')
				const $invoiceExportDateInput = $invoiceExportDate.find('input.invoice_dates')
				$invoiceExportDateInput.on('click touchstart', e => {
				   flatpickr(e.currentTarget, {
					  mode: 'range',
					  dateFormat: 'M d, Y',
					  onChange(selectedDates, dateStr, instance) {
						 if(selectedDates.length === 2) { 
							const [ from, to ] = [
							   instance.formatDate(selectedDates[0], 'Y-m-d'),
							   instance.formatDate(selectedDates[1], 'Y-m-d')
							]
							 
							$invoiceExportDate.find('input[name="from"]').val(from)
							$invoiceExportDate.find('input[name="to"]').val(to)
						 }
	
						 $invoiceExportDateButton.prop('disabled', selectedDates.length === 2 ? false : true)
						 app.webkit.taptic()
					  },
					  onOpen(selectedDates, dateStr, instance) {
						 $(instance.element).data('flatpickr', instance)
						 $(instance.calendarContainer).removeClass('animate')
						 app.webkit.taptic()
					  }
				   }).open()
            })
            
            $invoiceExportDate.on('click', 'button.exportDate', async e => {
               e.originalEvent.preventDefault()
               const $button = $(e.currentTarget)
               const $form = $button.closest('form')
               const type = $button.data('type')
               const from = $form.find('input[name="from"]').val()
               const to = $form.find('input[name="to"]').val()

               
               $button.button()
               await app.post('request/invoice', 'get', {
                  from, to, master: master.id
               }, async invoices => {
                  if(!invoices || !invoices.length) {
                     $button.button(false)
                     return
                  }

                  switch(type) {
                     case 'invoice': {
                        await app.pdf.createTotal(invoices, from, to)
                        await app.pdf.save(`Invoice. ${master.fname} ${master.lname}. ${moment(new Date(from)).format('MMM D, YYYY')} - ${moment(new Date(to)).format('MMM D, YYYY')}`) 
                        break
                     }

                     case 'report': {
                        for(const invoice of invoices)
                           await app.pdf.createInvoice(invoice)
                        await app.pdf.save(`Report. ${master.fname} ${master.lname}. ${moment(new Date(from)).format('MMM D, YYYY')} - ${moment(new Date(to)).format('MMM D, YYYY')}`) 
                        break
                     }
                  } 

                  for(const invoice of invoices) {
                     if(invoice.status !== 4 && invoice.status !== 'exported') {
                        $(document).find(`[data-invoice-handler="${invoice.id}"]`).attr('data-status', 'exported')
                        .data('status', 'exported')

                        await app.socket.send({
                           action: 'invoice/change', 
                           invoice: invoice.id,
                           status: 'exported'
                        })
                     }
                  }
                  
                  $button.button(false)
               })
            })


            $invoice.on('submit', async e => {
               e.originalEvent.preventDefault()
               const $this = $(e.currentTarget) 
               const invoice = {}

               invoice['master'] = master.id
               $.each($this.serializeArray(), (i, e) => {
                  invoice[e.name] = e.value
               })

               $invoice_button.button()
               await app.planner.master.invoice.create(invoice, async data => {
                  if(!data) {
                     $invoice_button.button(false)
                     return
                  }

                  await app.planner.master.invoice.add({
                     money: invoice.money,
                     date: flatpickr.formatDate(flatpickr.parseDate(invoice.date, 'Y-m-d'), "M d, Y"),
                     comment: invoice.comment,
                     id: data[0].id
                  }, $master.find('.list.invoices'), true)

                  await this.$this.data('slinky').jump(`.master-${master.id} ul.vacation-block`)
                  await $invoice_button.button(false)
                  await app.ui.update()
               })
            })
            /* INVOICE UI */


            /* VACATION DATE RANGE UI */
            const $vacationLoad = $master.find('.load-vacation')
            const $vacation =  $master.find('form[name="vacation"]')
            const $vacationButton = $vacation.find('button')
            const $vacationDate = $vacation.find('.vacation_dates')

            $vacationDate.on('click', e => {
               flatpickr($vacationDate[0], {
                  mode: 'range',
                  dateFormat: 'M d, Y',
                  minDate: new Date().fp_incr(1),
                  onChange(selectedDates, dateStr, instance) {
                     if(selectedDates.length === 2) { 
                        const [ from, to ] = [
                           instance.formatDate(selectedDates[0], 'Y-m-d'),
                           instance.formatDate(selectedDates[1], 'Y-m-d')
                        ]
                        
                        $vacation.find('input[name="from"]').val(from)
                        $vacation.find('input[name="to"]').val(to)
                     }

                     $vacationButton.prop('disabled', selectedDates.length === 2 ? false : true)
                     app.webkit.taptic()
                  },
                  onOpen(selectedDates, dateStr, instance) {
                     $(instance.element).data('flatpickr', instance)
                     $(instance.calendarContainer).removeClass('animate')
                     instance.set('disable', master.vacation)
                     app.webkit.taptic()
                  }
               }).open()
            })

            $vacationLoad.on('mousedown', async e => {
               e.originalEvent.preventDefault()
               const $this = $(e.currentTarget)
               const $vacations = $this.closest('li').find('.list')
               $vacations.html('')
                  .addClass('loading')

               await app.post('user/vacation', 'get', { master: master.id }, async data => master.vacation = data)

               setTimeout(() => {
                  for(const vacation of master.vacation)
                     app.planner.master.vacation.add(vacation, $master.find('.list.vacations'))
               
                  $vacations.removeClass('loading')
                  this.$this.height(this.$this.find('ul.active').height())
               }, 300)
            })

            $vacation.on('submit', async e => {
               e.originalEvent.preventDefault()
               const $this = $(e.currentTarget) 
               let vacation = {}

               vacation['master'] = master.id
               for(const e of $this.serializeArray()) 
                  vacation[e.name] = e.value

               $vacationButton.button()
               await app.planner.master.vacation.create(vacation, async vacation => {
                  if(!vacation)
                     return

                  master.vacation.push(vacation)
                  $vacationDate.val('')

                  await app.planner.master.vacation.add(vacation, $master.find('.list.vacations'), true)
                  this.$this.find('ul.active > .header > .back').trigger('click')
                  await app.ui.update()
               })
               $vacationButton.button(false)
            })
            /* VACATION DATE RANGE UI */

            await this.rebuild()
         } catch(e) {
            app.error('popover.master > add', e)
         }
      },

      async remove(master = false) {
         try {
            if($(`ul.master-${master}.active`).length > 0)
               this.$this.data('slinky').home()
            
            const $master = $(document).find(`.master__${master}`)
            if($master.data('marker'))
               $master.data('marker').setMap(null)
            $master.remove()
            $(document).find(`[data-master-handler="${master}"]`).remove()
         } catch(e) {
            app.error('popover.master > remove', e)
         }
      },

      async rebuild() {
         try {
            const slinky = this.$this.data('slinky') || false

            if(!slinky)
               return

            await slinky.rebuild()  
            await app.ui.update()
         } catch(e) {
            app.error('popover.master > rebuild', e)
         }
      }
   }
})()


import { invoices } from './module/invoices'
masters.invoinces = invoices
import { documents } from './module/documents'
masters.documents = documents
import { edit } from './module/edit'
masters.edit = edit
import { pricing } from './module/pricing'
masters.pricing = pricing
import { vacations } from './module/vacations'
masters.vacations = vacations