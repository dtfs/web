import { app } from '../../../../app/'
 
export const manager = (() => {
 	'use strict'

 	return {
		async create(manager = false) {
			try {
				if(!manager)
					return 

				await app.planner.popover.managers.add(manager)
			} catch(e) {
				app.error('master > create', e)
			}
		}, 

		async change(id = false, param = false, value = false, additional = {}) {
			try {
				const $param = $(document).find(`[data-manager-${id}-${param}]`) || false
				$param.html(value)
			} catch(e) {
				app.error('master > change', e)
			}
		},

		async remove(manager = false) {
			try { 
				if(!manager)
					return  

				await app.planner.popover.managers.remove(manager)
			} catch(e) {
				app.error('master > create', e)
			}
		}
 	}
 })()