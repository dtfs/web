import { app } from '../../../app/'
import { planner } from '..'
  
export const socket = (() => {
	'use strict'

	return {
		async connect(callback) {
		 	try {
				const wss = new WebSocket("wss://dtfs.pro:8080")

				wss.onmessage = async e => {
					const data = JSON.parse(e.data)
					const $request = $(`.order__${data.request}`) || false
					const $badge = $request.find('.history')
					const muuri = $request.closest('.muuri').data('muuri') || false

					switch(data.action) {
						case 'vendor/create': {
							app.planner.vendor.create(data.vendor)
							break
						}
						case 'vendor/change': {
							await app.planner.vendor.change(data.vendor, data.param, data.value, data.additional)
							break
						}
						case 'vendor/remove': {
							app.planner.vendor.remove(data.manager)
							break
						}

						case 'manager/create': {
							app.planner.manager.create(data.manager)
							break
						}
						case 'manager/change': {
							await app.planner.manager.change(data.manager, data.param, data.value, data.additional)
							break
						}
						case 'manager/remove': {
							app.planner.manager.remove(data.manager)
							break
						}

						case 'master/create': {
							app.planner.master.create(data.master)
							break
						}
						case 'master/change': {
							await app.planner.master.change(data.master, data.param, data.value, data.additional)
							break
						}
						case 'master/remove': {
							app.planner.master.remove(data.master)
							break
						}


						case 'master/vacation/create': {
							await app.planner.master.vacation.add(data.vacation)
							break
						}

						case 'master/vacation/remove': {
							await app.planner.master.vacation.remove(data.vacation)
							break
						}

						case 'invoice/change': {
							if(data.status) {
								app.request.report.status(data.invoice, data.status)
							} else if(data.data) {
								app.request.report.update(data.data)
							}
							break
						}

						case 'dragStart': {
							$badge.button()
							$request.addClass('disabled')
							$request.removeClass('draggable')
							break
						}

						case 'dragEnd': {
							$badge.button(false)
							$request.removeClass('disabled')
							$request.addClass('draggable')
							break
						}

						case 'move': {
							$badge.button()
							$request.addClass('disabled')
							$request.removeClass('draggable')

							if(data.to && data.to.date && data.to.master) { 
								await app.request.load(data.request, $(`.day.${data.to.date}`), false, e => {
									e.position = data.to.position
									return e
								})
							} else
								await app.request.load(data.request, $(`.backlog`).find(`.orders`))
							break
						}

						case 'create': {
							await app.request.load(data.request, $(`.backlog`).find(`.orders`))
							break
						}

						case 'remove': {
							await planner.request.remove($request)
							break
						}

						case 'position': {
							muuri.move($request[0], data.value)
							await planner.master.update($request.closest('.master'))
							break
						}

						case 'change': {
							await app.request.ui.update(data.request, data.param, data.value)
							await planner.master.ui.update($(`.order__${data.request}`).closest('.master'))
							break
						}

						case 'connection': {
							app.socket.send({
								connection: data.id,
								type: 'account',
								value: app.user.type(),
								id: app.storage.get('id'),
								session: app.storage.get('session')
							})
							break
						}
					}
				}

				wss.onopen = async e => {
					socket.wss = wss
					app.socket = socket

					if(callback)
						callback(true)
				}

				wss.onclose = async e => {
					if(!e.wasClean) {
						$('.plan').addClass('loading')
						setTimeout(() => app.reconnect(), 3000)
					}
				}

				wss.onerror = async e => {
					if(callback)
						callback(false)

					await app.socket.close()
				}
			} catch(e) {
				app.error('planner.socket.connect', e)
			}
		},


		async send(data = false) {
			if(!data) return
			const request = data.request || false

			if(request) {
				const $request = $(`.order__${data.request}`)
				if($request.length > 0) {
					data.user = $request.data('user')
					data.master = $request.data('master')
					data.vendor = $request.data('vendor')
				}
			}

			socket.wss.send(JSON.stringify(data))
		},

		async close() {
			await socket.wss.close()
		}
	}
})()
