import { app } from '../../app/'
const scrollintoview = require('jquery.scrollintoview')


export const marker = (() => {
	'use strict'

	const toggleBounce = $marker => {
		$marker.setAnimation(google.maps.Animation.BOUNCE)
		$marker.setAnimation(null)
	}

	return {
		create($map = $('#map')[0], data) {
			try {
				const id = data.id
				const $request = $(`.order__${id}`)
				const position = data.position
				const type = data.type || false
				let $marker = false

				switch(type) {
					case 'master': {
						$marker = new google.maps.Marker({
							map: $map,
							position,
							id,
							icon: {
								url: `/dist/cache/avatar/${id % 26}.svg`,
								scaledSize: new google.maps.Size(32, 32)
							}
						})
						/* Action listners */
						const $master = data.master || false

						if($master) {
							$marker.addListener('mouseover', async e => $master.addClass('hover').scrollintoview())
							$marker.addListener('mouseout', e => $master.removeClass('hover'))

							$master.on('mousedown touchstart', async e => {
								if(!$master.hasClass('routed'))
									app.map.direction.create($master.find('.orders'))
							})

							$master.find('.name').on('mousedown', e => $map.setCenter($marker.position))
						}
						break
					}


					default: {
						$marker = new google.maps.Marker({
							position, map: $map, id,
							icon: `/dist/cache/map/${data.type}.svg`
						})

						/* Action listners */
						$marker.addListener('mouseover', e => $request.addClass('hover').scrollintoview())
						$marker.addListener('mouseout', e => $request.removeClass('hover'))

						$marker.addListener('click', async e => {
							if(e.wa.shiftKey && app.user.type() === 'admin') {
								const $master = $('.master.routed')
								const $requests = $master.find('.orders')
								const master = $master.data('data').id
								const date = $master.closest('.day').data('date')

								await app.request.change(id, { param: 'master', value: master })
								await app.request.change(id, { param: 'date', value: date })
								await app.request.change(id, { param: 'status', value: 1 })

								await app.socket.send({
									type: 'move', id,
									to: { master: master, date: date }
								})

								await app.request.load(id, $requests).then(async () => {
									await app.planner.master.update($master)
								})
							} else
								toggleBounce($marker)
						})

						$request
							.on('mouseover', e => $marker.setAnimation(google.maps.Animation.BOUNCE))
							.on('mouseout', e => $marker.setAnimation(null))
							.on('mousedown', e => $map.setCenter(data.position))
					}
				}

				return $marker
			} catch(e) {
				app.error('marker.create', e)
			}
		},


		change($marker, data) {
			try {
				if(!$marker)
					return false

				$marker.setIcon(`/dist/cache/map/${data.type}.svg`)
			} catch(e) {
				app.error('marker.change', e)
			}
		},

		async position(marker = false, lat = false, lng = false) {
			try {
				await app.map.direction.remove()
				marker.setPosition(new google.maps.LatLng(lat, lng))
			} catch(e) {
				app.error('marker.change', e)
			}
		}
	}
})()
