import { app } from '../../app/'

const moment = require('moment')
const Direction = new google.maps.DirectionsService()
const DirectionDisplay = new google.maps.DirectionsRenderer({
	suppressMarkers: true,
	preserveViewport: true,
	polylineOptions: {
		strokeColor: '#6059f7',
   	strokeWeight: 2
	}
})


export const direction = (() => {
	'use strict'

	return {
		async create($block = false, optimize = false, $map = false, callback) {
			try {
				await this.remove()

				if($map === false && !app.is.mobile())
					$map = $('#map').data('map')

				const $master = $block.closest('.master')
				const $marker = $master.data('marker') || false
				const $requests = $master.find('.orders')
				const $orders  = $requests.find('.order')
				const route = []
				

				if($master.data('vacation') || $master.hasClass('vacation') || $master.length === 0 || $block.length === 0)
					return

				$.each($orders, (i, request) => route.push({
					location: $(request).data('marker').position,
					stopover: true
				}))

				const start = {
					location: app.user.type() === 'master' ? app.settings.marker.position : $master.data('marker').position,
					stopover: true
				}

				/* Create direction */
				if(start.location !== null && route.length > 0) {
					if($map) 
						DirectionDisplay.setMap($map)

					$master.find('canvas').addClass('loading')
					Direction.route({
						origin: start.location,
						destination: start.location,
						waypoints: route,
						travelMode: 'DRIVING',
						optimizeWaypoints: optimize,
					}, async (response, status) => {
						$master.find('canvas').removeClass('loading')

						if(status !== 'OK') {
							app.error('map.direction.create', status)
							return
						}

						$master.addClass('routed')
						DirectionDisplay.setDirections(response)

						let total = 0
						for(const e of response.routes[0].legs)
							total += e.distance.value
						const miles = Math.round(total / 1600 * 100) / 100

						$master.find('.total').html(`<i data-feather="map-pin"></i> ${miles} mi.`)
						$master.data('miles', miles)

						const baseMiles = 0
						const earn = Math.round((miles - baseMiles) * .45 * 100) / 100
						const esimate = earn > 0 ? earn : 0
						$master.find('.money').html(`<i data-feather="dollar-sign"></i> ${esimate}`)

						$.each($orders, async (i, request) => {
							i = response.routes[0].waypoint_order[i]
							const $request = $(request)
							const distance = response.routes[0].legs[i + 0].distance || ''
							const duration = response.routes[0].legs[i + 0].duration || '' 
							const $marker = $request.data('marker')

							$request.find('.distance').html(distance.text)
							$request.find('.duration').html(duration.text)

							/* CHANGE TIME-FRAME AND SAVE */
							const prevStart = $request.prev().find('.time.from').val() || '8:00 AM'
							const timeStart = i === 0 ? '08:00 AM' : moment(prevStart, 'LT').add(duration.value / 60 + 30, 'm').format('LT')
							const timeEnd = moment(timeStart, 'LT').add(120, 'm').format('LT')

							switch($request.data('status')) {
								case 'new':
								case 'changed': {
									if(app.user.type(app.storage.get('type')) === 'admin') {
										if($request.find('.time.from').val() !== timeStart) {
											$request.find('.time.from').val(timeStart)
										 	app.request.change($request.data('id'), { param: 'start', value: timeStart })
										}

										if($request.find('.time.to').val() !== timeEnd) {
											$request.find('.time.to').val(timeEnd)
											app.request.change($request.data('id'), { param: 'end', value: timeEnd })
										}

										/* SAVE DISTANCE */
										if(parseInt($request.data('distance')) !== parseInt(distance.value))
											app.request.change($request.data('id'), { param: 'distance', value: distance.value })
										/* SAVE DISTANCE */
									}
									break
								}
							}
							/* END CHANGE TIME-FRAME */
							

							
							if($map && $marker)
								$marker.setMap($map)
							app.map.marker.change($marker, {type: i + 1})
						})
						/* END CHANGE POSITION */

						if(callback)
							await callback(response)

						app.ui.update()
						app.planner.master ? app.planner.master.ui.update($master) : false
					})
				} else {
					$master.find('.total')
						.html(`<i data-feather="map-pin"></i> 0 mi.`)
					$master.find('.money')
						.html(`<i data-feather="dollar-sign"></i> 0`)
				}

				app.ui.update()
			} catch(e) {
				app.error(`map.direction.create`, e)
			}
		},


		async remove() {
			try {
				$('.master').removeClass('routed')
				DirectionDisplay.setMap(null)

				for(const request of $('.day').find('.order')) {
					const $marker = $(request).data('marker') || false
					if($marker) 
						$marker.setMap(null)
				}
			} catch(e) {
				app.error(`map.direction.remove`, e)
			}
		}
	}
})()
