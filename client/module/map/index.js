import { app } from '../app'

export const map = (() => {
	'use strict'

	return {
		async init($map, callback) {
			try {
				const map = await new google.maps.Map($map[0], {
					center: {
						lat: 34,
						lng: -84
					},
					zoom: 8,
					disableDefaultUI: true,
					mapTypeControlOptions: {
						mapTypeIds: ['styled_map']
					}
				})

				map.mapTypes.set('styled_map', this.StyledMapType)
				map.setMapTypeId('styled_map')
				$map.data('map', map)

				if (callback)
					callback(map)

				return map
			} catch (e) {
				app.error(`Map.init`, e)
			}
		}
	}
})()

import {
	direction
} from './module/direction'
map.direction = direction

import {
	marker
} from './module/marker'
map.marker = marker


map.StyledMapType = new google.maps.StyledMapType([{
		"featureType": "administrative.locality",
		"elementType": "all",
		"stylers": [{
				"hue": "#2c2e33"
			},
			{
				"saturation": 7
			},
			{
				"lightness": 19
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "landscape",
		"elementType": "all",
		"stylers": [{
				"hue": "#ffffff"
			},
			{
				"saturation": -100
			},
			{
				"lightness": 100
			},
			{
				"visibility": "simplified"
			}
		]
	},
	{
		"featureType": "poi",
		"elementType": "all",
		"stylers": [{
				"hue": "#ffffff"
			},
			{
				"saturation": -100
			},
			{
				"lightness": 100
			},
			{
				"visibility": "off"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "geometry",
		"stylers": [{
				"hue": "#bbc0c4"
			},
			{
				"saturation": -93
			},
			{
				"lightness": 31
			},
			{
				"visibility": "simplified"
			}
		]
	},
	{
		"featureType": "road",
		"elementType": "labels",
		"stylers": [{
				"hue": "#bbc0c4"
			},
			{
				"saturation": -93
			},
			{
				"lightness": 31
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "road.arterial",
		"elementType": "labels",
		"stylers": [{
				"hue": "#bbc0c4"
			},
			{
				"saturation": -93
			},
			{
				"lightness": -2
			},
			{
				"visibility": "simplified"
			}
		]
	},
	{
		"featureType": "road.local",
		"elementType": "geometry",
		"stylers": [{
				"hue": "#e9ebed"
			},
			{
				"saturation": -90
			},
			{
				"lightness": -8
			},
			{
				"visibility": "simplified"
			}
		]
	},
	{
		"featureType": "transit",
		"elementType": "all",
		"stylers": [{
				"hue": "#e9ebed"
			},
			{
				"saturation": 10
			},
			{
				"lightness": 69
			},
			{
				"visibility": "on"
			}
		]
	},
	{
		"featureType": "water",
		"elementType": "all",
		"stylers": [{
				"hue": "#e9ebed"
			},
			{
				"saturation": -78
			},
			{
				"lightness": 67
			},
			{
				"visibility": "simplified"
			}
		]
	}
])