import { app } from '../../app'
  
export const ui = (() => {
	'use strict'

	return {
		async init() {
			try {
				$(document).on('click', '.request--create', e => {
					e.originalEvent.preventDefault()
					app.request.create($(e.currentTarget).closest('.form'))
				}) 

				$(document).on('click', '.order--delete', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					$this.button()
					await app.request.remove($this.data('id'), async data => {
						$this.button(false)
					})
				})
				
				$(document).on('click', '.order--archive', e => app.request.archive($(e.currentTarget).data('id')))
				$(document).on('click', '.order--change', e => app.request.change($(e.currentTarget).data('id')))
			} catch(e) {
				app.error('request.ui.init')
			}
		},

		async update(id, param, value) {
			try {
				const $request = $(`.order__${id}`)
				const $badge = $request.find('.badge.history')
				const muuri = $request.closest('.muuri').data('muuri')
				
				if($request.data(param) === value)
					return

				$request.data(param, value)
				$request.attr(`data-${param}`, value)

				switch(param) {
					case 'status': {
						$request.data(param, app.request.status(value))
						$request.attr(`data-${param}`, app.request.status(value))

						$badge.html(app.request.status(value))
							.attr('class', '')
							.addClass(`badge history ${app.request.status(value)}`)

						if(value > 1)
							$request.removeClass('draggable')
						else
							$request.addClass('draggable')
						break
					}

					case 'start': {
						$request.find('input.from').val(value)
						break
					}
					
					case 'end': {
						$request.find('input.to').val(value)
						break
					}
				}
 
				app.planner.muuri.update(muuri)
				setTimeout(() => app.planner.muuri.update(muuri), 300)
			} catch(e) {
				app.error('request.ui.update', e)
			}
		}
	}
})()
