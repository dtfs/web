import { app } from '../../app/'
 

export const report = (() => {
	'use strict'

	return {
		async change(request = false, data = false) {
			try {
				if(!request) {
					app.error('Report.change')
					return
				}

				let report = {}
				let ready = false

				$.each(data, (i,e) => {
					if(e.name !== 'request[]' && e.name !== 'type[]') {
						report[e.name] = e.value
						if(e.value.length === 0)
							ready = true
					}
				})

			
				this.update(data)
				await app.post('request/invoice', 'change', {
					request,
					report
				}, async data => { 
					await app.socket.send({
						action: 'invoice/change', 
						master: data.master,
						data
					})
				})

				return ready
			} catch(e) {
				app.error('Report.change', e)
			}
		},


		async load(id = false, $block) {
			try {
				await app.post('request/invoice', 'get', { request: id }, async invoice => {
					$.each(JSON.parse(invoice.report), (key, value) => {
						$block.find(`[name="${key}"]`).val(value)
					})
				})
			} catch(e) {
				app.error('Report.load', e)
			}
		},

		update(data = false) {
			if(!data)
				return

			for(const param in data) {
				if($(document).find(`input[name="${param}"], select[name="${param}"], textarea[name="${param}"]`).length > 0)
					for(const block of $(document).find(`input[name="${param}"], select[name="${param}"], textarea[name="${param}"]`))
						$(block).val(data[param])
				
				if($(document).find(`[data-${param}-change="${data.id}"]`).length > 0)
					for(const block of $(document).find(`[data-${param}-change="${data.id}"]`))
						$(block).html(data[param])
			}
		},

		status(id = false, status = false) {
			for(const invoice of $(document).find(`[data-invoice-handler="${id}"]`)) {
				$(invoice).data('status', status)
				$(invoice).attr('data-status', status)
			}
		}
	}
})()



