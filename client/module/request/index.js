import { app } from '../app/'
const geocoder = new google.maps.Geocoder()
 
export const request = (() => {
	'use strict'

	return {
		async init() {
			try {
				await this.ui.init()
			} catch(e) {

			}
		},

		/* Function desc */
		async create($form = false) {
			try {
				if(!$form)
					return
					
				const $button = $form.find('button')
				const data = {category: $form.data('category') }
				
				for (const input of $form.find('input, select, textarea')) {
					const name = $(input).attr('name')
					const value = $(input).val()

					if (name === 'phone')
						data[name] = `1${$form.find('input[name="phone"]').cleanVal()}`
					else
						data[name] = value

					$form.find(`input[name="${name}"], textarea[name="${name}"]`).val('')
				}

				$button.button()
				geocoder.geocode({ address: data.address }, async (response, status) => {
					if(status !== 'OK') {
						$button.button(false)
						app.ui.alert(`Can't find the specified address, please try again`)
						return
					}

					data.address = response[0].formatted_address
					data.lat = response[0].geometry.location.lat()
					data.lng = response[0].geometry.location.lng()

					await app.post('request', 'create', data, async id => {
						$button.button(false)
						if(id.error) {
							app.error(`request.create`, id)
							return
						}

						await this.load(id)
						await app.socket.send({ action: 'create', request: id })
						WebuiPopovers.hideAll()
					})
				})
			} catch(e) {
				app.error(`request.create`, e)
			}
		},


		async load(type, $block = $(document).find(`.backlog .orders`), callback = false, precallback = false) {
			try {
				const id = type
				const date = $($block).closest('.day').data('date')
				type = `${app.user.type()}/${type}`

				await app.post('request', 'get', {
					id, type, date
				}, async data => {
					switch(id) {
						case 'backlog': {
							if(app.planner.muuri)
								await app.planner.muuri.init($block)
 
							$.each(data, async (i, item) => {
								item.distance = {
									value: item.distance,
									text: `${item.distance / 1600} mi`
								}

								if(app.user.type() === 'admin') {
									item.drag = 'drag'
									item.draggable = 'draggable'
								}

								await app.planner.request.add($block, item)
							})
							break
						}

						case 'day': {
							$($block).html('')
							$.each($(document).find('.master'), (i,e) => {
								const marker = $(e).data('marker') || false

								if(marker)
									marker.setMap(null)
							})

							$.each(data, async (i, master) => {
								switch(app.user.type()) {
									case 'admin': {
										await app.planner.master.add(master)
										break
									}

									default: {
										await app.planner.request.add($block, master)
									}
								}
							})

							if(app.user.type() === 'master') {
								if(app.planner.muuri)
									await app.planner.muuri.init($($block))
							}

							break
						}

						default: {
							if(precallback)
								data = precallback(data)
								
							if((data.status < 3)) {
								data.drag = 'drag'
								data.draggable = 'draggable'
							}

							if($($block).hasClass('day')) {
								const $master = $block.find(`.master__${data.master}`)
								await app.planner.request.add($master.find('.orders'), data)
							} else
								await app.planner.request.add($block, data)
						}
					}

					if(callback)
						await callback()

					await app.ui.update()
				})

			} catch(e) {
				app.error('Request.load', e)
			}
		},

		/* Function desc */
		async change(id = false, data = false, callback) {
			try {
				if(!id || !data)
					return

				const param = data.param
				let value = data.value
				const $request = $(`.order__${id}`)
				const $badge = $request.find('.badge.history')
				data['id'] = id

				$badge.button()
				await app.post('request', 'change', data, async data => {
					if(!data) {
						app.error(`request.change`, data)
						return
					}

					/* Sending notification to master that he was removed from request */
					if(param === 'master')
						await app.socket.send({
							action: 'remove',
							request: id,
							master: $(`.order__${id}`).data('master') || null
						})

					await app.request.ui.update(id, param, value)
					await app.socket.send({
						action: 'change',
						request: id,
						param,
						value
					})

					if(callback)
						callback(data)
				})
				$badge.button(false)
			} catch(e) {
				app.error(`request.change`, e)
			}
		},


		/* Function desc */
		async remove(id, callback = false) {
			try {
				await app.post('request', 'remove', { id }, async data => {
					WebuiPopovers.hideAll()

					const $request = $(`.order__${id}`)
					if($request.closest('.orders').data('muuri'))
						await app.planner.request.remove($(`.order__${id}`))
					else
						await $request.remove()

					await app.socket.send({
						action: 'remove',
						request: id
					})

					if(callback) 
						await callback(data)
				})
			} catch(e) {
					app.error(`request.remove`, e)
			}
		},

		async cancel(id, callbacl = false) {
			try {
				await this.change(id, { param: 'status', value: 6 })
				WebuiPopovers.hideAll()
				
			} catch(e) {
				app.error('request > cancel', e)
			}
		}
	}
})()


import { ui } from './module/ui'
request.ui = ui
import { report } from './module/report'
request.report = report
import { cancel } from './module/ui'
request.cancel = cancel


request.status = type => {
	switch (type) {
		case 1: return 'changed'; break
		case 2: return 'waiting'; break
		case 3: return 'ready'; break
		case 4: return 'working'; break
		case 5: return 'done'; break
		case 6: return 'cancel'; break
		case 7: return 'archive'; break
		case 'changed': return 1; break
		case 'waiting': return 2; break
		case 'ready': return 3; break
		case 'working': return 4; break
		case 'done': return 5; break
		case 'cancel': return 6; break
		case 'archive': return 7; break
		default: return 'new'
	}
}
