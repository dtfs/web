import { app } from '..'

require('web-animations-js')

import Flickity from 'flickity'
import 'flickity-as-nav-for'

import jQueryBridget from 'jquery-bridget'
jQueryBridget('flickity', Flickity, $)

import flatpickr from 'flatpickr'
require('jquery-ui')

const Notyf = require('notyf/dist/notyf.min')
const notyf = new Notyf({ delay: 3000 })
const feather = require('feather-icons')
const mask = require('jquery-mask-plugin')
const webuiPopover = require('webui-popover')
const slinky = require('./slinky')
const fileupload = require('blueimp-file-upload')
const Hammer = require('hammerjs')
 
WebuiPopovers.setDefaultOptions({
	container: $(document).find('main')[0],
	trigger: 'click',
	multi: true,
	animation: 'pop',
	closeable: true,
	placement: 'auto',
	async onShow(e) {
		$('html').addClass('popin')

		await app.ui.update()
		await app.webkit.taptic()
		await app.sound.click()
	},
	async onHide(e) {
		$('html').removeClass('popin')
	}
})



export const ui = (() => {
	'use strict'

	return {
		async init() {
			try {
				$(document).on('click', '.load', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)
					await app.load($this.data('page'), $this.data('value'))
				})
			} catch(e) {
				app.error('app.ui.init', e)
			}
		},


		async update(page) {
			try {
				feather.replace()
				app.user.ui.update(page)

				$.each($('.slinky:not(.slinky-menu)'), async (i, e) => {
					const $this = $(e)
					
					await $this.data('slinky', $this.slinky({ 
						title: true, 
						speed: app.storage.get('animation') === 'false' ? 0 : 300 
					}))

					if(app.is.webview()) {
						const mc = new Hammer(e)
						mc.get('swipe').set({ 
							direction: Hammer.DIRECTION_HORIZONTAL, 
							threshold: 150,
							velocity: .3
						})
						mc.on("swiperight", e => {
							$this.scrollTop(0)
							$this.find('ul.active > .header > .back').trigger('click')
						})
					}
				})

				for(const phone of $('input[name="phone"]'))
					$(phone).mask('+1 (000) 000-0000')
				for(const phone of $('[data-phone-convert]'))
					$(phone).mask('+1 (000) 000-0000')


				for(const select of $('select')) {
					if($(select).data('type') === 'vendor')
						$(select).findSelect($(select).data('type'))
					else if($(select).data('type') === 'price')
						$(select).findService($(select).data('type'))
					else 
						$(select).val($(select).data('value'))
				}

				for(const date of $('input.date')) {
					$(date).on('mousedown', e => {
						flatpickr(e.currentTarget, {
							altInput: true,
							altFormat: "M d, Y", 
							dateFormat: "Y-m-d", 
							onOpen(selectedDates, dateStr, instance) {
								$(instance.element).data('flatpickr', instance)
								$(instance.calendarContainer).removeClass('animate')
							}
						}).open()
					})
				}

				$.each($('.animate'), (i,e) => {
					const $this = $(e)
					let animate = $this.data('animate') || 'fadeIn'

					if($this.hasClass('flatpickr-calendar')) {
						$this.removeClass('animate')
					} else {
						if(app.storage.get('animation') !== 'false')
						setTimeout(() => {
							$this.removeClass('animate')
								.animateCss(animate)
								.addClass('animated')
						}, i * 10)
						else
							$this.removeClass('animate')
					}
				})
			} catch(e) {
				app.error('app.ui.update', e)
			}
		},


		async popover($this, popover = false, data = {}) {
			try {
				let content = false
				if(!popover) 
					return

				if(popover === 'map')
					content = `
						<div class="map-ui animate"></div>
						<div class="map--button d-md-none"></div>
					`
				else
					content = await app.template(popover)(Object.assign(data, {
						sound: app.storage.get('sound') === 'true' ? true : false,
						animation: app.storage.get('animation') === 'false' ? false : true
					}))

				const closeable = $this.data('type') === 'webview' && app.is.webview() ? false : true
				await WebuiPopovers.hideAll()
				await $this.webuiPopover({
					content,
					width: $this.data('type') === 'webview' ? 380 : 320,
					animation: closeable ? app.is.mobile() ? false : 'fade' : false,
					trigger: 'manual',
					placement: $this.data('placement') || 'auto',
					cache: false,
					closeable, 
					async onShow(e) {
						e.focus()
						e.addClass('show')
						
						if (!app.is.mobile() && $this.data('type') === 'webview')
							e.css({
								'left': '64px',
								'top': 0,
								'margin': 0,
								'border-radius': 0,
								'min-height': '100%'
							})

						if(app.is.mobile() && e.find('.close').length > 0) {
							const mc = new Hammer(e.find('.close')[0])
							mc.get('pan').set({ 
								direction: Hammer.DIRECTION_VERTICAL, 
								threshold: 0 
							})
							mc.on("pan", _ => {
								if(_.deltaY > 0)
									e.css({ 'transform': `translateY(${_.deltaY}px)` })
							}).on("panend", _ => {
								if(_.deltaY > 200)
									WebuiPopovers.hideAll()
								else 
									e.css({ 'transform': 'translateY(0px)' })
							})
						}
				
						$('html').addClass('popin')
						if(popover === 'map') 
							$('html').addClass('fixed')

						if(closeable) {
							if(app.is.mobile())
								e.animateCss('slideInUp')
							await $('html').addClass('offtop')
							await app.webkit.light()
						}

						const $popover = $(e)
						const $content = $popover.find('.list')

						$content.addClass('loading') 
						$('.webview-home').removeClass('active')
						$this.addClass('active')
						$.each($(`.webview-menu > [data-popover="${popover}"]`), (i,e) => $(e).addClass('active'))
 
						switch(popover) {  
							case 'map': { 
								setTimeout(async () => await app.planner.popover.map($this, $popover), 150)
								break
							} 

							case 'admin/popover/masters': {
								await app.planner.popover.masters.init($popover.find('.webui-popover-content'))
								if($this.data('master'))
									await $popover.find('.slinky').data('slinky').jump(`ul.master-${$this.data('master')}`, false)
								break
							}

							case 'admin/popover/vendors': { 
								await app.planner.popover.vendors.init($popover.find('.webui-popover-content'))
								if($this.data('vendor'))
									await $popover.find('.slinky').data('slinky').jump(`ul.vendor-${$this.data('vendor')}`, false)
								break
							}

							case 'admin/popover/managers': {
								await app.planner.popover.managers.init($popover.find('.webui-popover-content'))
								if($this.data('managers'))
									await $popover.find('.slinky').data('slinky').jump(`ul.manager-${$this.data('manager')}`, false)
								break
							}

							case 'admin/me/order__menu': {
								let request = false
								await app.post('request', 'get', { id: $this.data('id') }, async data => request = data)
							
								switch(parseInt(request.status)) {
									case 1:
									case 2:
									case 3:
									case 4: {
										request.CHANGEABLE = true
										break
									}

									case 6: { 
										request.CHANGEABLE = true
										request.COPYABLE = true
										break
									}

									default: {  
										request.DELETEABLE = true
										request.CHANGEABLE = true  
										break
									}
								}

								const $content = $(app.template(popover)(request))
								const $cancel = $content.find(`[data-request-cancel]`)
								const $remove = $content.find(`[data-request-remove]`)
								$popover.find('.webui-popover-content').html($content)

								if($cancel.length > 0)
									$cancel.on('submit', async e => {
										e.originalEvent.preventDefault()
										const $this = $(e.currentTarget)
										const id = $this.data('request-cancel')
										const comment = $this.find('textarea[name="comment"]').val()

										$this.find('button').button()
										await app.request.change(id, {
											param: 'status',
											value: 6,
											comment
										}, async data => { 
											$this.find('button').button(false)
											WebuiPopovers.hideAll()
										})
									})


								if($remove.length > 0)
									$remove.on('submit', async e => {
										e.originalEvent.preventDefault()
										const $this = $(e.currentTarget)
										const id = $this.data('request-remove')

										$this.find('button').button()
										await app.request.remove(id)
									})
								break
							}

							case 'other/popover/search': {
								setTimeout(() => $popover.find('input[type="search"]').focus(), 100)
								break
							}

 
							case 'request/report': {
								const id = $this.data('id')
								const request = $(`.order__${id}`).data()
								let service = request.category
								let invoice = false


								switch(service) {
									case 'MI': {
										service = 'mattress'
										break
									}
						
									case 'FS': {
										service = 'furniture'
										break
									}
								}

								await app.post('request/invoice', 'get', { request: id }, async data =>  invoice = data)
								invoice.report = JSON.parse(invoice.report)


								
								const $report = $(app.template(`request/report/${service}`)(invoice))
								$popover.find('.webui-popover-content').html($report)

								$report.on('change', async e => {
									e.originalEvent.preventDefault()
									const $this = $(e.currentTarget)
									await app.request.report.change(id, $this.serializeArray())
								}).on('submit', async e => {
									e.originalEvent.preventDefault()
									const $this = $(e.currentTarget)
									const status = parseInt(app.request.status(request.status)) + 1

									$this.button()
									if(status < 6) {
										await app.request.change(request.id, {
											param: 'status',
											value: status
										})
									}
										
									WebuiPopovers.hideAll()
								})
 
								break
							}

							case 'request/cancel': {
								const id = $this.data('id')
								let cancel

								await app.post('request/cancel', 'get', { request: id }, async data => cancel = data)
								const $cancel = $(app.template(`request/cancel`)({ id }))
								$popover.find('.webui-popover-content').html($cancel)

								$cancel.on('submit', async e => {
									e.originalEvent.preventDefault()
									const $this = $(e.currentTarget)
									const comment = $this.find('textarea[name="comment"]').val()

									$this.find('button').button()
									await app.request.change(id, { param: 'status', value: 6, comment })
									$this.find('button').button(false)
									WebuiPopovers.hideAll()
								})

								break
							}
						}

						await app.ui.update()
						await app.webkit.taptic() 
						$content.removeClass('loading')
						$.each($popover.find('input[name="address"]'), (i,e) => $(e).googlePlaces())
					},

					async onHide(e) {
						$('.webview-home').addClass('active')
						$('html').removeClass('popin fixed offtop')
						
						$.each($(`[data-popover="${popover}"]`), (i,e) => $(e).removeClass('active'))
						$.each(e.find('input[name="address"]'), (i,e) => $(e).googlePlaces(false))
						$.each(e.find('.flatpickr-input'), (i,e) => $(e).data('flatpickr').destroy())
						
						await app.webkit.taptic()
						await app.webkit.dark()
						$this.webuiPopover('destroy')
					}
				}).webuiPopover('show')
			} catch(e) {
				app.error('ui.popover', e)
			}
		},

		async alert(message = false) {
			if(message)
				notyf.alert(message)
		},


		async confirm(message = false) {
			if(message)
				notyf.confirm(message)
		}
	}
})()
