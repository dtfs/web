import { app } from '..'
 
export const handler = (() => {
	'use strict'

	return {
		init() {
			try {
				$(document).on('keyup change', 'input, textarea, select', e => {
					const $form = $(e.currentTarget).closest('form, .form')
					const $button = $form.find('button')
					const form = $form.find('input, select, textarea')
					let disabled = false

					for(const input of form) 
						if($(input).val().length === 0 && $(input).attr('name') !== 'files[]') { 
							app.debug($(input).attr('name'), $(input).val().length)
							disabled = true 
						}
					
					$(e.target).addClass('changed')
					$button.prop('disabled', disabled)
				}) 

				$(document).on('mouseup', '.slinky:not(.main)', e => {
					const $this = $(e.currentTarget)
					const $list = $this.closest('.list').find('.slinky')

					$.each($list, (i,e) => {
						if(e === $this[0])
							return

						const slinky = $(e).data('slinky')
						slinky.home()
					})
				})

				$(document).on('click', '.next, .back', e => {
					const $this = $(e.currentTarget)
					const $slinky = $this.closest('.slinky.main')
				
					setTimeout(() => {
						const $active = $($slinky.find('ul.active')[0])
						$slinky.height($active.height())
					}, 300)
				})
				
				$(document).on('click', e => {
					if(!app.storage.get('sound'))
						app.storage.set('sound', true)
				})

				$(document).on('click', 'main > .container-fluid, .webview-home', e => WebuiPopovers.hideAll())

				$(document).on('focusout', 'input, select, textarea', e => {
					if(app.is.webview() && !$(e.relatedTarget).is('.form-control'))
						$('html, body').scrollTop(0)
				})


				$(document).on('change', 'input.sound[type="checkbox"]', e => {
					const $this = $(e.currentTarget)
					app.storage.set('sound', $this.is(':checked'))
				})

				$(document).on('change', 'input.animation[type="checkbox"]', e => {
					const $this = $(e.currentTarget)
					app.storage.set('animation', $this.is(':checked'))
				})

				$(document).on('click', 'a[data-view]', e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)
					const view = $this.data('view') || 'full'
					const $day = $('.day.is-selected')

					$('.plan').attr('data-view', view)
					$.each($day.find('.orders'), async (i, e) => {
						const muuri = $(e).data('muuri') || false
						await app.planner.muuri.update(muuri)
						setTimeout(() => $('.days').flickity('resize'), 300)
					})
				})

				$(document).on('click', 'a, button, .button, .flatpickr-next-month, .flatpickr-prev-month', e => app.sound.click())
				$(document).on('mouseenter', 'button, a, .button, input', e => app.sound.hover2())

				$(document).on('click', '.load', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)
					await app.load($this.data('page'), $this.data('value'))
				})


				$(document).on('click', '[data-popover]', async e => {
					e.originalEvent.preventDefault()
					const $this = $(e.currentTarget)

					await app.ui.popover($this, $this.data('popover'), $this.data('data'))
				})
			} catch(e) {
				app.error('handler.init', e)
			}
		}
	}
})()
