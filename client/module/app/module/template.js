import { app } from '..'


export const templateLoader = (() => {
  'use strict'

  // States of loaded templates
  const STATES = {
    unloaded: 0,
    loaded: 1,
    error: -1
  }

  // Registations data store
  const registrations = {}

  // Internal class for storing registrations
  const registration = function (key, path, html) {
    this.key = key
    this.path = path
    this.html = html
    this.state = html ? STATES.loaded : STATES.unloaded
  }

  // Loads the templates html
  const load = (reg, async) => {
    // only load if in unloaded state
    if (reg.state == STATES.unloaded) {
      $.ajax({
        url: reg.path,
        async: async,
        success(data) {
          reg.html = data
          reg.state = STATES.loaded
          return reg.html
        },
        error(data) {
          reg.state = STATES.error
          console.log(data)
        }
      })
    }
  }

  return {
    register(obj, loadSync) {
      try {
        // do some validation
        if (!obj.key)
          throw "templates.register requires a key value"
        if (!(obj.path || obj.data))
          throw "templates.register requires a path or data value"

        // do registration
        const reg = new registration(obj.key, obj.path, obj.data)
        registrations[reg.key] = reg

        // optionally load stuff
        load(reg, loadSync ? false : true)
      } catch (e) {
        app.error('tempalteLoader > html', e)
      }
    },

    html(key) {
      try {
        // check for bad states
        if (!registrations[key])
          throw false
        if (registrations[key].state == STATES.error)
          throw "Template load failure"

        const reg = registrations[key]

        // load if necessary
        if (reg.state === STATES.unloaded)
          load(reg, false)

        return reg.html
      } catch (e) {
        app.error('tempalteLoader > html', e)
      }
    }
  }
})()


templateLoader.register({
  key: 'home',
  path: `/client/template/page/home.html`
})
templateLoader.register({
  key: "other/loader",
  path: "/client/template/other/loader/index.html"
})

templateLoader.register({
  key: "other/popover/search",
  path: "/client/template/other/popover/search.html"
})
templateLoader.register({
  key: "other/popover/more",
  path: "/client/template/other/popover/more.html"
})

templateLoader.register({
  key: "request/report/furniture",
  path: "/client/template/other/report/furniture/index.html"
})
templateLoader.register({
  key: "request/report/mattress",
  path: "/client/template/other/report/mattress/index.html"
})
templateLoader.register({
  key: "request/cancel",
  path: "/client/template/other/cancel/index.html"
})

templateLoader.register({
  key: "me/admin",
  path: "/client/template/admin/index.html"
})
templateLoader.register({
  key: "admin/menu",
  path: "/client/template/admin/popover/menu/index.html"
})

templateLoader.register({
  key: "me/master",
  path: "/client/template/master/index.html"
})
templateLoader.register({
  key: `master/menu`,
  path: `/client/template/master/popover/menu/index.html`
})