import { app } from ".."

export const sound = (() => {
	'use strict'
	const hover = new Audio('/dist/cache/sound/hover-ui-3.ogg')
	const click = new Audio('/dist/cache/sound/hover-ui-1.ogg')
	const lock = new Audio('/dist/cache/sound/hover-ui-2.ogg')
	const hover2 = new Audio('https://moment-zero.com/assets/sounds/tick.mp3')

	return {
		async hover() {
			hover.volume = .02

			if(app.storage.get('sound') === 'true' && !app.is.mobile())
				await hover.play()
		},

		async hover2() {
			hover2.volume = .02

			if(app.storage.get('sound') === 'true' && !app.is.mobile())
				await hover2.play()
		},

		async click() {
			click.volume = .2

			if(app.storage.get('sound') === 'true' && !app.is.mobile())
				await click.play()
		},

		async lock() {
			lock.volume = .2

			if(app.storage.get('sound') === 'true')
				await lock.play()
		}
	}
})()
