
export const storage = (() => {
	'use strict'

	return {
		set(name, data) {
			try {
				window.localStorage.setItem(name, data)
			} catch(e) {
				app.error('app.storage.set')
			}
		},

		get(name) {
			try {
				return window.localStorage.getItem(name) || false
			} catch(e) {
				app.error('app.storage.get')
			}
		},

		remove(name) {
			try {
				window.localStorage.removeItem(name)
			} catch(e) {
				app.error('app.storage.remove')
			}
		}
	}
})()
