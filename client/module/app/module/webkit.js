import { app } from '..'
 
export const webkit = (() => {
	'use strict'

	return {
		taptic() {
			try {
				if(!window.webkit) 
					return

				window.webkit.messageHandlers["taptic"].postMessage(true)
			}	catch(e) {
				//app.error('webkit taptic', e)
			}
		},

		dark() {
			if(!window.webkit) 
				return

			window.webkit.messageHandlers["dark"].postMessage(true)
		},

		light() {
			if(!window.webkit) 
				return

			window.webkit.messageHandlers["light"].postMessage(true)
		},

		safari(link = false) {
			try {
				if(!window.webkit || !link || link === '#0') 
					return

				window.webkit.messageHandlers["safariLink"].postMessage(link)
			}	catch(e) {
				app.ui.alert(e)
				//app.error('webkit safari', e)
			}
		},

		intercomInit() {
			try {
				if(!window.webkit) 
					return

				window.webkit.messageHandlers["intercomInit"].postMessage(app.storage.get('id'))
			}	catch(e) {
				app.ui.alert(e)
			}
		},

		intercomShow() {
			try {
				if(!window.webkit) 
					return

				window.webkit.messageHandlers["intercomShow"].postMessage(true)
			}	catch(e) {
				app.ui.alert(e)
			}
		},

		error(message) {
			try {
				if(!window.webkit) 
					return
				
				window.webkit.messageHandlers["error"].postMessage(message)
			} catch(e) {
				//app.error('webkit error', e)
			}
		} 
	}
})() 



$(document).on('click', 'a, button, .button, .dates span, .flatpickr-day', e => webkit.taptic())
$(document).on('focus', 'input, textarea, select', e => webkit.taptic())
$(document).on('click', 'a[target="_blank"]', e => app.webkit.safari($(e.currentTarget).attr('href') || false))