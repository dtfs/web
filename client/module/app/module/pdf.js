import { app } from '..'
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import moment from 'moment'

export const pdf = (() => {
    'use strict'

    return {
		async create() {
			this.doc = new jsPDF({
				lineHeight: 1.4
			})
		},

		async createTotal(invoices = false, from, to) {
			this.doc ? this.doc.addPage() : this.create()
			let left = 20
			const body = []
			let total = 0

			this.doc.setFontSize(40)
			this.doc.setFontType('bold')
			this.doc.setTextColor('#000')
			this.doc.text(left, 30, `INVOICE`)

			/* BILLED TO */
			this.doc.setFontSize(10)
			this.doc.setFontType('normal')
			this.doc.setTextColor('#e3e3e3')
			this.doc.text('Billed To', left , 45)

			this.doc.setFontSize(12)
			this.doc.setTextColor('#000')
			this.doc.text(invoices[0].request.vendor.company_name, left, 53)
			this.doc.text(this.doc.splitTextToSize(invoices[0].request.vendor.address, 70), left, 61)
			/* BILLED TO */


			/* INVOICE INFO */
			this.doc.setFontSize(10)
			this.doc.setFontType('normal')
			this.doc.setTextColor('#e3e3e3')
			this.doc.text('Date From', 190, 45, null, null, 'right')

			this.doc.setFontSize(12)
			this.doc.setTextColor('#000')
			this.doc.text(`${moment(new Date(from)).format('MMM D, YYYY')}`, 190, 53, null, null, 'right')

			
			this.doc.setFontSize(10)
			this.doc.setFontType('normal')
			this.doc.setTextColor('#e3e3e3')
			this.doc.text('Date To', 190, 63, null, null, 'right')

			this.doc.setFontSize(12)
			this.doc.setTextColor('#000')
			this.doc.text(`${moment(new Date(to)).format('MMM D, YYYY')}`, 190, 71, null, null, 'right')
			/* INVOICE INFO */
			

			/* TABLE */	
			body.push([ 
				{ content: `#`, styles: { fontStyle: 'bold' } },
				{ content: `Code`, styles: { fontStyle: 'bold' } },
				{ content: `Date`, styles: { fontStyle: 'bold' } },
				{ content: `Service`, styles: { fontStyle: 'bold' } },
				{ content: `Client`, styles: { fontStyle: 'bold' } },
				{ content: `Price`, styles: { halign: 'right', fontStyle: 'bold' } }
			])

			for(const invoice of invoices) {
				body.push([ 
					invoice.request.id, 
					invoice.request.sc, 
					moment(new Date(invoice.request.date)).format('MMM D, YYYY'),
					invoice.service_name,
					invoice.request.name,
					{ content: `$${invoice.price},00`, styles: { halign: 'right' } }
				])

				total += invoice.price
			}

			body.push([
				{ content: `Total`, colSpan: 5, styles: { fontStyle: 'bold' } },
				{ content: `$${total},00`, styles: { halign: 'right', fontStyle: 'bold' } }
			])

			this.doc.setTextColor('#000')
			this.doc.autoTable({
				startY: 85,
				margin: { top: 30, left: 20, right: 20, bottom: 30 },
				styles: { 
					cellPadding: 4,
					fontSize: 12,
					textColor: '#000'
				},
				alternateRowStyles: {
					fillColor: '#f9f9f9' 
				},
				body
			})
			/* TABLE */
		},

        async createInvoice(invoice = false) {
            try {
				this.doc ? this.doc.addPage() : this.create()
                let left = 20

 
                this.doc.setFontSize(40)
                this.doc.setFontType('bold') 
				this.doc.text(left, 30, `ORDER #${invoice.request.id}`)

				/* BILLED TO */
                this.doc.setFontSize(10)
                this.doc.setFontType('normal')
                this.doc.setTextColor('#e3e3e3')
                this.doc.text('Company', left , 45)

                this.doc.setFontSize(12)
                this.doc.setTextColor('#000')
                this.doc.text(invoice.request.vendor.company_name, left, 53)

                this.doc.setFontSize(10)
                this.doc.setFontType('normal')
                this.doc.setTextColor('#e3e3e3')
                this.doc.text('Service Code', left, 63)

                this.doc.setFontSize(12)
                this.doc.setTextColor('#000')
                this.doc.text(invoice.request.sc, left, 71)
                /* BILLED TO */


                /* INVOICE INFO */
                this.doc.setFontSize(10)
                this.doc.setFontType('normal')
                this.doc.setTextColor('#e3e3e3')
                this.doc.text('Date', left + 50, 45)

                this.doc.setFontSize(12)
                this.doc.setTextColor('#000')
                this.doc.text(`${moment(new Date(invoice.request.date)).format('MMM D, YYYY')}`, left + 50, 53)

				
                this.doc.setFontSize(10)
                this.doc.setFontType('normal')
                this.doc.setTextColor('#e3e3e3')
                this.doc.text('Client', left + 50, 63)

                this.doc.setFontSize(12)
                this.doc.setTextColor('#000')
                this.doc.text(invoice.request.name, left + 50, 71)
				/* INVOICE INFO */
				

                /* ADDRESS */
                this.doc.setFontSize(10)
                this.doc.setFontType('normal')
                this.doc.setTextColor('#e3e3e3')
                this.doc.text('Address', left + 100, 45)

                this.doc.setFontSize(12)
                this.doc.setTextColor('#000')
                this.doc.text(this.doc.splitTextToSize(invoice.request.address, 70), left + 100, 53)
				/* ADDRESS */

				this.doc.setDrawColor('#f2f2f2')
				this.doc.line(20, 83, 190, 83)

				/* REPORT */
				this.doc.setFontSize(40)
                this.doc.setFontType('bold')
				this.doc.text(left, 105, `REPORT`)

                this.doc.autoTable({
					startY: 115,
					margin: { top: 30, left: 20, right: 20, bottom: 30 },
					styles: { 
						cellPadding: 4,
						fontSize: 12,
						textColor: '#000'
					},
					alternateRowStyles: {
						fillColor: '#f9f9f9' 
					},
                    body: [
                        [ 
							{ content: 'Service', styles: { cellWidth: 50 } }, 
							{ content: invoice.service_name , styles: { cellWidth: 120 } }
						],
                        [ 'Product', invoice.report.product ],
                        [ 'Rate condition', invoice.report.rate_condition ],
                        [ 'Damage Code', invoice.report.damage_code ],
                        [ 'Source of damage', invoice.report.damage_source ],
                        [ 'Work performed', invoice.report.work_performed ],
                        [ 'Part order', invoice.report.part_order ],
						[ 'Comment', invoice.report.comment ]
                    ]
                })
				/* REPORT */

				const load = url => new Promise(resolve => {
					const img = new Image()
  					img.crossOrigin = 'Anonymous'
					img.onload = () => resolve({ img, ratio: img.naturalWidth / img.naturalHeight })
					img.src = url
				})

				const { img, ratio } = await load('https://images.unsplash.com/photo-1566597270866-4ce134fc6e5d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=3150&q=10')
				const base64Img = await app.base64(img)

				this.doc.addPage()
				app.pdf.doc.addImage(base64Img, 'JPEG', 0, 0, 210, 210 / ratio);
            } catch(e) {
                app.error('pdf > create', e)
            }
		},
		
		async save(name = false) {
			if(app.is.webview()) {
				await $.post('/server/module/request/file/base64.php', {
					base64: this.doc.output('datauristring').replace('data:application/pdf;filename=generated.pdf;base64,', ''),
					name: app.storage.get('session')
				}, async data => {
					app.webkit.safari(`https://dtfs.pro/server/module/request/file/cache/${app.storage.get('session')}.pdf`)
					setTimeout(() => $.post('/server/module/request/file/base64.php', {
						name: app.storage.get('session')
					}), 10000)
				})
			} else
				await this.doc.save(`${name}.pdf`)
			
			this.doc = false
		}


    }
})()