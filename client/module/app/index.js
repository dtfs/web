export const DEBUG = true  
export const VERSION = '0.9.15' 
  
const Handlebars = require('handlebars')
import { templateLoader } from './module/template'
import { planner as adminPlanner } from '../planner/admin/'
import { planner as masterPlanner } from '../planner/master/'
import { planner as userPlanner } from '../planner/user/'
import { planner as vendorPlanner } from '../planner/vendor/'

export const app = (() => {
	'use strict'
 
	return {
		async init() {
			try { 

				if(app.is.webview() || app.is.mobile())
					$('html').addClass('webview')

				await this.user.init()  
				await this.request.init()
				await this.handler.init()
			} catch(e) {
				app.error('app.init', e)
			}
		},

		/* User interface update or init*/
		async load(page, value = null, callback) {
			try {
				const $page = $('#page')  
				WebuiPopovers.hideAll()

				$page.html(app.template(page, value))
				$('body').attr('class', '').addClass(page.replace('/', ' '))

				if(page === 'home') {
					await app.webkit.taptic()
					await app.ui.update(page)
					return
				}

				switch(page) { 
					case 'me/admin':
						app.planner = adminPlanner
						break

					case 'me/master':
						app.planner = masterPlanner
						break
 
					case 'me/user':
						app.planner = userPlanner
						break
 
					case 'me/vendor':
						app.planner = vendorPlanner
						break
				}

				if(app.planner)
					await app.planner.init()
				await app.webkit.taptic()
				await app.ui.update(page)
				await window.Intercom('update') 

				$('.load').removeClass('active')
				$(`*[data-page="${page}"]`).addClass('active')

				if(!app.is.mobile())
					$('.desktop-remove').remove()

				if(callback)
					await callback($page)
			} catch(e) {
				app.error('app.load', e)
			}
		},

		async post(get = false, action = false, data = false, callback) {
			try {
				if(!get || !action)
					return false

				await $.post('/server/api/', {
					get, do: action, data
				}, async data => {
					if(data && data.error) {
						app.ui.alert(data.error)
						this.error(`${get}/${action}`, data)
						data = false
					} 

					if(callback)
						await callback(data)
				})
			} catch(e) {
				this.error(`app.post > ${get}/${action}`, e)
			}
		},

		/* System functions */
		template(key = false, data = {}) {
			try {
				if(templateLoader.html(key))
					return Handlebars.compile(templateLoader.html(key))
				else 
					return Handlebars.compile(templateLoader.html('other/loader'))
			} catch(e) {
				app.error(`app.template > ${key}`, e)
			}
		},

		async error(name, message) {
			this.debug(name, message)
		},

		async debug(name, message) {
			if(DEBUG) {
				app.webkit.error(message)
				console.group(name)
				console.log(message)
				console.groupEnd()
			}
		},

		async reconnect() { 
			console.error('RECONNECT')

			await app.planner.socket.connect(async e => {
				if(!e) 
					return

				if($(document).find('.backlog').length > 0) {
					await app.request.load('backlog', $('.backlog .orders')[0])
					await app.request.load('day', $('.day.is-selected').find('.masters')[0]) 
				} else {
					await app.request.load('day', $('.day.is-selected').find('.orders')[0])
				}

				$('.plan').removeClass('loading')
			})
		},

		async base64(img) {
			const canvas = document.createElement('CANVAS')
			const ctx = canvas.getContext('2d')
			canvas.height = img.naturalHeight
			canvas.width = img.naturalWidth
			ctx.drawImage(img, 0, 0)

			return canvas.toDataURL()
		}
	}
})()


app.VERSION = VERSION

import { ui } from './module/ui'
app.ui = ui
import { is } from './module/is'
app.is = is
import { storage } from './module/storage'
app.storage = storage
import { sound } from './module/sound'
app.sound = sound
import { handler } from './module/handler'
app.handler = handler
import { webkit } from './module/webkit'
app.webkit = webkit

import { user } from '../user/'
app.user = user
import { request } from '../request/'
app.request = request
import { map } from '../map/'
app.map = map
import { pdf } from './module/pdf'
app.pdf = pdf

