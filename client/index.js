import 'flatpickr/dist/themes/airbnb.css'
import 'notyf/dist/notyf.min.css'
import 'animate.css'
import './scss/index.scss'
 
const moment = require('moment')
import { app } from './module/app/'
window.app = app
$(() => app.init())


$.fn.extend({
    animateCss(animationName, callback) {
      const animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend'
      this.addClass(`animated ${animationName}`).one(animationEnd, function() {
          $(this).removeClass(`animated ${animationName}`)

          if(callback)
            callback()
      })
      return this
    },


	button(action) {
		const $this = $(this)
		if(action === false) {
			$this.removeClass('loading')
			$this.prop('disabled', false)
		} else {
			$this.addClass('loading')
			$this.prop('disabled', true)
		}
	},


	async googlePlaces(action = true) {
		try {
			const $address = $(this)
			const $form = $address.closest('.form')
			const $zipcode = $form.find('input[name="zipcode"]') 
			const $utc = $address.closest('form').find('input[name="utc"]')
			const $lat = $form.find('input[name="lat"]') 
			const $lng = $form.find('input[name="lng"]') 
			$zipcode.mask('00000-000')

			if(!action) {
				$(".pac-container").remove()
				return
			}

			if($address.hasClass('inited'))
				return

			const place_changed = place => {
				if(!place)
					return

				$address.val('')
				$zipcode.val('')
				$lat.val('')
				$lng.val('')

				const lat = place.geometry.location.lat()
				const lng = place.geometry.location.lng()
				$lat.val(lat).addClass('changed')
				$lng.val(lng).addClass('changed')
				
				$utc.val(place.utc_offset).addClass('changed')
				$address.val(place.formatted_address).addClass('changed')
				
				if(place.address_components)
					for (var i = 0; i < place.address_components.length; i++) {
						switch(place.address_components[i].types[0]) {
							case 'postal_code':
								$zipcode.val(place.address_components[i].long_name).addClass('changed')
						}
					}
			}

			$address.autocomplete = new google.maps.places.Autocomplete(($address[0]), {
				types: ['address'], componentRestrictions: { country: 'US' }
			})
			$address.autocomplete.addListener('place_changed', e => place_changed($address.autocomplete.getPlace()))

			$zipcode.autocomplete = new google.maps.places.Autocomplete(($zipcode[0]), {
				types: ['address'], componentRestrictions: { country: 'US' }
			})
			$zipcode.autocomplete.addListener('place_changed', e => place_changed($zipcode.autocomplete.getPlace()))
 
			$address.addClass('inited')
		} catch(e) {
			app.error('app', e)
		}
	},

	async findSelect(type) {
		try {
			const $this = $(this)
			if($this.hasClass('inited'))
				return
				
			$this.addClass('inited')
			await app.post('user', 'get', { type }, async data => {
				for(const vendor of data) {
					$this.append(`<option value="${vendor.id}">${vendor.company_name}</option>`)

					if(parseInt(vendor.id) === parseInt($this.data('value')))
						$this.val(vendor.id)
				}
			})
		} catch(e) {
			app.error('app', e)
		}
	},


	async findService(type) {
		try {
			const $this = $(this)
			if($this.hasClass('inited'))
				return

			$this.addClass('inited')
			await app.post('user/price', 'service', { 
				user: $this.data('user'),  
				category: $this.data('category') 
			}, async data => {
				for(const price of data.price) {
					$this.append(`<option value="${price.id}">${price.name} - $${price.price / 100}</option>`)

					if(parseInt(price.id) === parseInt($this.data('value')))
						$this.val(price.id)
				}

			})
		} catch(e) {
			app.error('app', e)
		}
	},


	/* History of request changes */
	async requestHistory() {
		const $badge = $(this)
		let setTimeoutConst

		$badge.on('mouseout', () => clearTimeout(setTimeoutConst))
		$badge.on('mouseover', e => {
			const $this = $(e.currentTarget)

			setTimeoutConst = setTimeout(async () => {
				if (!$this.hasClass('loading')) {
					$this.button()

					await app.post('request', 'history', {
						id: $this.closest('.order').data('id')
					}, async data => {
						$this.webuiPopover({
							placement: 'bottom-right',
							width: 320,
							content() {
								let $content = $(`<nav class="order__history mt-webview pt-5 pt-md-0"></nav>`)
								if (data === null)
									$content.addClass('empty')
								else
									$.each(data, (i, e) => {
										if(!e.comment)
											e.comment = ''

										const $action = $(`<div class="${e.status} small">
												${e.action}
												<span class="float-right ml-4">
													${moment.utc(e.time).local().format('MM/DD/Y, h:mm A')}
												</span>
												<small class="comment">
													${e.comment}
												</small>
											</div>`)

										if(e.media) {
											const $gallery = $('<div class="gallery border-0 p-0 mt-2" style="min-height: 0"></div>')
											$.each(e.media, (i, media) => {
												$gallery.append(`<img src="/dist/cache/request/${media.url}" />`)
											})

											$action.append($gallery)
										}



										if (e.status)
											$action.addClass('status')
										$content.append($action)
									})
								return $content
							},
							onHide() {
								$('html').removeClass('popin')
								$this.button(false)
								$this.removeClass('hover')
								$this.webuiPopover('destroy')
							}
						}).webuiPopover('show')

						await app.sound.click()
					})
				}
			}, 500)
		})
	}
})
