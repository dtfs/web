const webpack = require('webpack')
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    "bundle": ['babel-polyfill', './client/index.js'],
  },
  output: {
    filename: "app.js",
    sourceMapFilename: "app.js.map",
  }, 
  cache: true,
  devtool: 'source-map',
  stats: {
    warnings: false
  },
  target: 'node',
	optimization: {
     minimize: false
   },
  module: {
    rules: [{
      test: /\.m?js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }, {
      test: /(\.css|\.scss)$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: 'css-loader!sass-loader'
      })
    }, {
      test: /\.(woff|woff2|eot|ttf|otf)$/i,
      loader: "file-loader",
      query: {
        name: 'cache/[name].[ext]',
        useRelativePath: false
      }
    }, {
      test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.wav$|\.mp3$/,
      loaders: 'file-loader',
      query: {
        name: 'cache/[name].[ext]',
        useRelativePath: false
      }
    }, {
      test: /\.json$/,
      loader: 'json-loader'
    }]
  },
  plugins: [
    new ExtractTextPlugin('app.css'),
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery'
    }),
    new HtmlWebpackPlugin({
      hash: true,
      filename: '../index.html',
      template: './client/template/index.html'
    })
  ]
}
