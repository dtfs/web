<?php

class Bird extends App {

	public function __construct() {
    	parent::__construct();
		$this->MessageBird = $this->debug ? 'U8YnjVdH6rIiHBsbUfNn7ZNC5' : 'aBx1gVZ2U6cSs11d95cLpAIi0';
		$this->MessageBirdOriginator = '14704279101';
		$this->MessageBird = new \MessageBird\Client($this->MessageBird);
  	}

	public function send($data) {
		$send = new stdClass();
		$send->type = $data['type'] ?? false;
		$send->phone = $data['to'] ?? false;
		$send->message = $data['message'] ?? false;

		if(
			$send->phone === '19213457895'
			|| $send->phone === '19213457894' // master
			|| $send->phone === '19213457893' // vendor
			|| $send->phone === '19213457892' // user
		)
			$send->phone = '79213457895';

		if( 
			$send->phone === '19163300744'
			|| $send->phone === '19163300743' // master
			|| $send->phone === '19163300742' // vendor
			|| $send->phone === '19163300741' // user
		)
			$send->phone = '19163300744';


		switch($send->type) {
			case 'voice': {
				$Message = new \MessageBird\Objects\VoiceMessage();
				$Message->body = $send->message;
				$Message->language = 'en-us';
				$Message->voice = 'female'; 

				$Message->recipients = array($send->phone);
				if(!$this->MessageBird->voicemessages->create($Message))
					return false;

				$Message = new \MessageBird\Objects\Message();
				$Message->originator = $this->MessageBirdOriginator;
				$Message->body = $send->message;

				$Message->recipients = array($send->phone);
				if(!$this->MessageBird->messages->create($Message))
					return false;

				break;
			}

			default: {
				$Message = new \MessageBird\Objects\Message();
				$Message->originator = $this->MessageBirdOriginator;
				$Message->recipients = array($send->phone);
				$Message->body = $send->message;
				if(!$this->MessageBird->messages->create($Message))
					return false;
			}
		}

		return true;
	}
}
