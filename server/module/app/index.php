<?php
class App {
	/* MAIN CONSTRUCTOR */
	function __construct() {
		$this->debug = false;
		$this->hash = 'A_SDAS34$%+@R)_DFKPVCSS#$)@#@%$RFDSV#+)_$@#';

		$servername = "localhost";
		$username   = "root";
		$password   = "Parfaitshamer927113";
		$dbname     = "app";
		$this->db = new mysqli($servername, $username, $password, $dbname);
		mysqli_set_charset($this->db, "utf8");
		date_default_timezone_set('UTC');

		if(!isset($_SESSION))
			session_start();

		$this->id = $_SESSION['id'] ?? NULL;
		$this->type = $_SESSION['type'] ?? NULL;
		$this->session = $_SESSION['session'] ?? NULL;
	}
	/* MAIN CONSTRUCTOR */

	/* AUTOLOADER */
	public function autoload($class) {
		$split = explode('/', $class);
		include $_SERVER["DOCUMENT_ROOT"].'/server/module/' .$class. '/index.php';

		$class = end($split);
		return new $class();
	}
	/* AUTOLOADER */

	/* ACTION HANDLER */
	public function action($data = false) {
		$action = new stdClass();
		$action->type = $data['type'] ?? false;
		$action->action = $data['action'] ?? NULL;
		$action->user = $this->id ?? NULL;
		$action->session = $this->session ?? NULL;

		$action->param = $data['param'] ?? NULL;
		$action->value = $data['value'] ?? NULL;

		if(!$action->type || !$action->action)
			return false;

		switch($action->type) {
			case 'request': {
				$action->request = $data['request'] ?? false;
				$action->status = $data['status'] ?? NULL;
				$action->comment = $data['comment'] ?? NULL;

				$sql = "INSERT INTO request_action (
					request,
					session,
					action,
					status,
					comment,
					param,
					value
				) VALUES (
					'$action->request',
					'$action->session',
					'$action->action',
					'$action->status',
					'$action->comment',
					'$action->param',
					'$action->value'
				)";

				if(!$this->db->query($sql))
					return false;

				break;
			}

			default: {
				$sql = "INSERT INTO user_session_action (session, action) VALUES ('$this->session', '$action->action')";
				if(!$this->db->query($sql))
						return false;
			}
		}

		return true;
	}
	/* ACTION HANDLER */

	/* REQUEST TYPES */
	public function status($status) {
		switch ($status) {
			case 1:
				return 'changed';
				break;
			case 2:
				return 'waiting';
				break;
			case 3:
				return 'ready';
				break;
			case 4:
				return 'working';
				break;
			case 5:
				return 'done';
				break;
			case 6:
				return 'cancel';
				break;
			case 7:
				return 'archive';
				break;
			default:
				return 'new';
		}
	}
	/* REQUEST TYPES */

	/* USER TYPES */
	public function type($type) {
		switch ($status) {
			case 1:
				return 'admin';
				break;
			case 2:
				return 'master';
				break;
			case 3:
				return 'vendor';
				break;
			default:
				return 'user';
		}
	}
	/* USER TYPES */
}
