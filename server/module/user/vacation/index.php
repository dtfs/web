<?php
 
class Vacation extends App {

   public function __construct() {
      parent::__construct();
   }
   
   public function get($data = false) {
      $return = new stdClass();
      $return->data = [];

      $vacation = new stdClass();
      $vacation->master = $data['master'] ?? $this->id ?? false;
      $vacation->id = $data['id'] ?? false;
      
     if($vacation->id) {
      $sql = "SELECT id,
               date_from AS 'from', 
               date_to AS 'to', 
               comment,
               user AS 'master'
         FROM user_vacation 
         WHERE id = '$vacation->id'";
     } else {
      $sql = "SELECT id,
               date_from AS 'from', 
               date_to AS 'to', 
               comment,
               user AS 'master'
         FROM user_vacation 
         WHERE user = '$vacation->master'
         ORDER BY date_from DESC";
     }
      
      $result = $this->db->query($sql);

      if(!$result) {
         $return->error = $this->db->error;
         return $return;
      }

      $i = 0;
      while($row = $result->fetch_assoc()) {
         $return->data[$i] = $row;
         ++$i;
      }

      return $return->data;
   }

   public function create($data = false) {
      $return = new stdClass();

      $vacation = new stdClass();
		$vacation->master = $data['master'] ?? false;
		$vacation->from = $data['from'] ?? false;
		$vacation->to = $data['to'] ?? false;
      $vacation->comment = $data['vacation_comment'] ?? '';
      
      if(!$vacation->from || !$vacation->to || !$vacation->master)
         return $vacation;

      $sql = "INSERT INTO user_vacation (
            user, date_from, date_to, comment
         ) VALUES (
            '$vacation->master', '$vacation->from', '$vacation->to', '$vacation->comment'
         )";

      if(!$this->db->query($sql) || !$this->db->insert_id) {
         $return->error = 'Oops, looks like something goes wrong.';
         $return->errorDesctiption = $this->db->error;
         return $return;
      }
      
      $return = $vacation;
      $return->id = $this->db->insert_id;
      return $return;
   }

   public function remove($data = false) {
      $vacation = new stdClass();
      $vacation->id = $data['id'] ?? false;
      if(!$vacation->id)
         return false;

      $sql = "DELETE FROM user_vacation WHERE id = '$vacation->id'";
      if(!$this->db->query($sql))
            return false;
      
      return true;
   }
}