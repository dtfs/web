<?php

class User extends App {
	
	public function __construct() {
		parent::__construct();
		$this->Vacation = $this->autoload('user/vacation');
		$this->Price = $this->autoload('user/price');
	}
	  
	public function create($data = false) {
		$Bird = $this->autoload('bird');
		$return = new stdClass();

		$user = new stdClass();
		$user->type = $data['type'] ?? 0;
		//$user->vendor = $data['vendor'] ?? '';
		$user->phone = $data['phone'] ?? '';
		$user->email = $data['email'] ?? '';
		$user->company_name = $data['company_name'] ?? '';
		$user->fname = $data['fname'] ?? '';
		$user->lname = $data['lname'] ?? '';

		$user->address = $data['address'] ?? '';
		$user->zipcode = $data['zipcode'] ?? '';
		$user->optional = $data['optional'] ?? '';
		$user->utc = $data['utc'] ?? '';
		$user->lat = $data['lat'] ?? '';
		$user->lng = $data['lng'] ?? '';

		if(!$user->phone && !$user->email)
			return false;

		$sql = "INSERT INTO user (
			type, phone, fname, lname, address, zipcode, optional, utc, lat, lng, company_name, email
		) VALUES (
			'$user->type',  
			'$user->phone', 
			'$user->fname',
			'$user->lname', 
			'$user->address', 
			'$user->zipcode', 
			'$user->optional', 
			'$user->utc', 
			'$user->lat', 
			'$user->lng',
			'$user->company_name',
			'$user->email'
		)";

		if(!$this->db->query($sql)) {
			$return->error = $this->db->error;
			return $return;
		}

		if($user->phone) 
			$return->data = $Bird->send([
				'phone' => $user->phone,
				'message' => 'Hi, ' . $user->fname . ', we have created an account for you. Go to https://dtfs.pro for sign in, using your current phone number.'
			]);

		return $this->db->insert_id;
	}

	public function get($data = false) {
		$user = new stdClass();
		$user->id = $data['id'] ?? $this->id ?? false;
		$user->type = $data['type'] ?? false;

		$user->param = $data['param'] ?? false;
		$user->value = $data['value'] ?? false;

		switch($user->type) {
			case 'master': {
				$sql = "SELECT * FROM user WHERE type = 2 ORDER BY id ASC";
				$result = $this->db->query($sql);
				$i = 0;
				while($row = $result->fetch_assoc()) {
					$return[$i] = $row;
					$return[$i]['pricing']['mile'] = 0.1;
					$return[$i]['pricing']['fs'] = 10;
					$return[$i]['pricing']['mi'] = 10;
					$return[$i]['vacation'] = $this->Vacation->get(['master' => $row['id']]);
					++$i;
				}
				return $return;
				break;
			}

			case 'manager': { 
				$sql = "SELECT * FROM user WHERE type = 1 AND id != '$this->id'";
				$result = $this->db->query($sql);
				$i = 0;
				while($row = $result->fetch_assoc()) {
					$return[$i++] = $row;
				}
				return $return;
				break;
			}

			case 'vendor': {
				$sql = "SELECT * FROM user WHERE type = 3 AND vendor IS NULL";
				$result = $this->db->query($sql);
				$i = 0;
				while($row = $result->fetch_assoc()) {
					$id = $row['id'];
					$return[$i] = $row;

					$sqlSub = "SELECT * FROM user WHERE type = 3 AND vendor = '$id'";
					$resultSub = $this->db->query($sqlSub);

					if ($resultSub->num_rows > 0) {
						$iSub = 0;
						while($row = $resultSub->fetch_assoc()) {
							$return[$i]['subvendor'][++$iSub] = $row;
						}
					}

					++$i;
				}
				return $return;
				break;
			}

			default: {
				$sql = "SELECT * FROM user WHERE $user->param = '$user->value'";

				$result = $this->db->query($sql);
				if(!$result) {
					$return->error = $this->db->error;
					return $return;
				}

				return $result->num_rows > 0 ? $result->fetch_assoc() : false;
			}
		}

		return false;
	}


	public function change($data = false) {
		$user = new stdClass();
		$user->id = $data['id'] ?? false;
		$user->param = $data['param'] ?? false;
		$user->value = $data['value'] ?? null;

		if (!$user->id ||
			 	!$user->param ||
			 	($user->id != $this->id && $this->type != 'admin')
		)
			return false;

		if(empty($user->value))
			$sql = "UPDATE user
							SET $user->param = NULL
							WHERE id = '$user->id'";
		else
			$sql = "UPDATE user
							SET $user->param = '$user->value'
							WHERE id = '$user->id'";

		if(!$this->db->query($sql))
			return $this->db->error;

		return true;
	}

	public function remove($data = false) { 
		$Bird = $this->autoload('bird');
		$user = new stdClass();
		$user->id = $data['id'] ?? false;
		$user->data = $this->get([ 'param' => 'id', 'value' => $user->id ]);

		$sql = "DELETE FROM user WHERE id = '$user->id'";
		if(!$this->db->query($sql))
			return $this->db->error;

		return true;
	}

	/* SIGNIN */
	public function signin($data) {
		$bird = parent::autoload('bird');
		$return = new stdClass();
		$user = new stdClass();
		$user->phone = $data['phone'] ?? false;
		$user->code = $data['code'] ?? false;

		/* System, remove later */
		if(!$user->phone)
			return false;
		if($user->phone === '9213457895')
			$user->phone = '79213457895';
		else
			$user->phone = '1'.$user->phone;
		/* System, remove later */

		if(!empty($user->phone) && !$user->code) {
			/* Check if account exist */
			if(!$this->get([ 'param' => 'phone', 'value' => $user->phone ])) {
				$return->error = "Looks like you don't have access to our system. Please, contact support for more information.";
				return $return;
			}
			/* Check if account exist */

			$user->code = rand(1000, 9999);
			$user->message = 'Your code: ' . $user->code;

			$sql = "INSERT INTO user_session_code (phone, code) VALUES ('$user->phone', '$user->code')";
			if(!$this->db->query($sql) || !$bird->send(['to' => $user->phone, 'message' => $user->message]))
				return false;

		} else if(!empty($user->phone) && !empty($user->code)) {
			$sql = "SELECT * FROM user_session_code WHERE phone = '$user->phone' && code = '$user->code'";
			$result = $this->db->query($sql);

			if ($result->num_rows === 0) { 
				$return->error = "Invalid code, please, try again or get a new code.";
				return $return;
			}

			$sql = "SELECT * FROM user WHERE phone = '$user->phone'";
			$result = $this->db->query($sql);
			$row = $result->fetch_assoc();
			$user->id = $row['id'];
			$user->type = $row['type'];

			$sql = "DELETE FROM user_session_code WHERE phone = '$user->phone'";
			if(!$this->db->query($sql))
					return false;

			if (!empty($_SERVER['HTTP_CLIENT_IP']))
				$user->ip = $_SERVER['HTTP_CLIENT_IP'];
			else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
				$user->ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			else
				$user->ip = $_SERVER['REMOTE_ADDR'] ?? false;


			$user->session = md5($user->id . $this->hash . $user->type);
			$sql = "INSERT INTO user_session (user, ip, session) VALUES ('$user->id', '$user->ip', '$user->session')";
			if(!$this->db->query($sql))
					return false;
		}
 
		return $user ?? true;
	}


	public function session($data = false) {
		$user = new stdClass();
		$user->id = $data['id'] ?? false;
		$user->session = $data['session'] ?? false;
		$user->type = $data['type'] ?? false;

		$sql = "SELECT * FROM user_session AS session
						INNER JOIN user ON user.id = session.user
						WHERE user.id = '$user->id'
									AND user.type = '$user->type'
									AND session.session = '$user->session'
						";
		$result = $this->db->query($sql);
		if ($result->num_rows == 0)
			return $this->db->error;

		$sql = "SELECT id, phone, type, address, lat, lng 
			FROM user 
			WHERE id = '$user->id'";
		$result = $this->db->query($sql);
		$row = $result->fetch_assoc();
		$user->data = $row;

		switch($row['type']) {
			case '1':
				$user->type = 'admin';
				break;
			case '2':
				$user->type = 'master';
				break;
			case '3':
				$user->type = 'vendor';
				break;
			default:
				$user->type = 'user';
		}

		$_SESSION['id'] = $user->id;
		$_SESSION['session'] = $user->session;
		$_SESSION['type'] = $user->type;
		$this->id = $user->id;
		$this->session = $user->session;
		$this->type = $user->type;

		return $user;
	}


}
