<?php

class Price extends App {
	
	public function __construct() {
		parent::__construct();
    }

    public function service($data = false) {
        $price = new stdClass();
        $price->category = $data['category'] ?? false;
        $price->user = $data['user'] ?? false;

        if($price->category) {
            $sql = "SELECT * FROM request_service_type WHERE id = $price->category";
            $result = $this->db->query($sql);

            if($this->db->error)
                return $this->db->error;

            $return = $result->fetch_assoc();
            $return['price'] = $this->get([
                'category' => $price->category,
                'user' => $price->user
            ]);
        } else {
            $sql = "SELECT * FROM request_service_type";
            $result = $this->db->query($sql);

            if($this->db->error)
                return $this->db->error;

            $i = 0;
            while($row = $result->fetch_assoc()) {
                $return[$i] = $row;
                $return[$i]['price'] = $this->get([
                    'category' => $row['id'],
                    'user' => $price->user
                ]);
                ++$i;
            }
        }

        return $return;
    }

    public function get($data = false) {
        $price = new stdClass();
        $price->category = $data['category'] ?? false;
        $price->user = $data['user'] ?? false;
        $price->id = $data['id'] ?? false;

        if($price->id) {
            $sql = "SELECT service.*,
            IFNULL(custom.price, service.price) AS price
            FROM request_service AS service 
            LEFT OUTER JOIN request_service_custom AS custom
                ON service.id = custom.service 
                    AND custom.user = '$price->user'
            WHERE service.id = '$price->id'";

            $result = $this->db->query($sql);
            if($this->db->error)
                return $this->db->error;

            $return = $result->fetch_assoc();
        } else {
            $sql = "SELECT service.*,
            IFNULL(custom.price, service.price) AS price
            FROM request_service AS service 
            LEFT OUTER JOIN request_service_custom AS custom
                ON service.id = custom.service 
                    AND custom.user = '$price->user'
            WHERE service.type = '$price->category'";

            $result = $this->db->query($sql);
            if($this->db->error)
                return $this->db->error;
    
            $i = 0;
            while($row = $result->fetch_assoc()) {
                $return[$i] = $row;
                ++$i;
            }
        }

        
        
        return $return;
    }
    
}