<?php
 
class Invoice extends App {
   
   public function __construct() {
      parent::__construct();
		$this->Request = $this->autoload('request');
   }
   
   public function get($data = false) {
      $return = new stdClass();
      $return->data = [];

      $invoice = new stdClass();
      $invoice->master = $data['master'] ?? false;
      $invoice->vendor = $data['vendor'] ?? false;
      $invoice->request = $data['request'] ?? false;
      $invoice->id = $data['id'] ?? false;
      
      if($invoice->id) {
         $sql = "SELECT *
            FROM request_invoice 
            WHERE id = '$invoice->id'";
      } else if($invoice->master) {
         $sql = "SELECT invoice.*
            FROM request_invoice AS invoice
            LEFT JOIN request 
               ON invoice.request = request.id 
            WHERE invoice.master = '$invoice->master' 
               AND (request.status = 5 OR invoice.request IS NULL)
            ORDER BY time DESC";
      } else if($invoice->vendor) {
         $sql = "SELECT i.*, r.vendor
            FROM request_invoice as i
            LEFT JOIN request as r ON i.request = r.id
            WHERE r.vendor = '$invoice->vendor'
               AND (i.status = 2 OR i.status = 4)
            ORDER BY time DESC";
      }
      
      $result = $this->db->query($sql);
      if(!$result) {
         $return->error = $this->db->error;
         return $return;
      }

      $i = 0;
      while($row = $result->fetch_assoc()) {
         $return->data[$i] = $row;
         
         $invoice->request = $row['request']; 
         $return->data[$i]['request'] = $this->Request->get([ 'id' => $invoice->request ]);

         if($return->data[$i]['master'] > 0)
            $return->data[$i]['master'] = $this->Request->User->get([
               'param' => 'id',
               'value' => $return->data[$i]['request']['master']
            ]);
         ++$i;
      }

      return $return->data;
   }

   public function create($data = false) {
      $return = new stdClass();

      $invoice = new stdClass();
		$invoice->master = $data['master'] ?? $this->id ?? false;
		$invoice->from = $data['date_from'] ?? false;
		$invoice->to = $data['date_to'] ?? false;
      $invoice->comment = $data['invoice_comment'] ?? '';
      
      if(!$invoice->from || !$invoice->to || !$invoice->master)
         return $invoice;

      $sql = "INSERT INTO request_invoice (
            user, date_from, date_to, comment
         ) VALUES (
            '$invoice->master', '$invoice->from', '$invoice->to', '$invoice->comment'
         )";

      if(!$this->db->query($sql))
            return $this->db->error;
            
      return $this->get([
         'master' => $invoice->master,
         'date_from' => $invoice->from,
         'date_to' => $invoice->to,
         'id' => $this->db->insert_id
      ]);
   }

   public function remove($data = false) {
      $invoice = new stdClass();
      $invoice->id = $data['id'] ?? false;
      if(!$invoice->id)
         return false;

      $sql = "DELETE FROM request_invoice WHERE id = '$invoice->id'";
      if(!$this->db->query($sql))
            return false;
      
      return true;
   }
}