<?php
 
class Request extends App {

	public function __construct() {
    	parent::__construct();
		$this->User = $this->autoload('user');
		$this->Bird = $this->autoload('bird');
  	}

	public function get($data = false, $return = false) {
		$request = new stdClass();

		$request->user = $this->id;
		$request->id = $data['id'] ?? false;
		$request->type = $data['type'] ?? false;
		$request->date = $data['date'] ?? false;

		$request->param = $data['param'] ?? false;
		$request->value = $data['value'] ?? false;



		switch($request->type) {
			case 'admin/backlog': {
				$sql = "SELECT *
								FROM request
								WHERE master IS NULL
									OR date IS NULL
									OR status IS NULL
								ORDER BY id DESC";
				$result = $this->db->query($sql);

				$i = 0;
				while($row = $result->fetch_assoc()) {
					$return[$i] = $row;
					$return[$i]['vendor'] = $this->User->get([
						'param' => 'id',
						'value' => $row['vendor']
						]);
					++$i;
				}

				return $return;
				break;
			}

			case 'admin/day': {
				$masters = $this->User->get([ 'type' => 'master' ]);
				
				foreach ($masters as &$master) {
					$id = $master['id'];

					$request_sql = "SELECT * FROM request
						WHERE master = '$id'
									AND date = '$request->date'
									#AND status != 6
						ORDER BY position ASC";

					$i2 = 0;
					$request_result = $this->db->query($request_sql);
					while($request_row = $request_result->fetch_assoc()) {
						$today = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
						$tomorrow = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d") + 1, date("Y")));

						$master['request'][$i2] = $request_row;
						$master['request'][$i2]['vendor'] = $this->User->get([
							'param' => 'id',
							'value' => $request_row['vendor']
						]);
						++$i2;
					}
			  }

				return $masters;
				break;
			}

			case 'master/day': {
				$sql = "SELECT * FROM request
						WHERE master = '$this->id'
							AND date = '$request->date'
						ORDER BY position ASC";

				$i = 0;
				$result = $this->db->query($sql);
				while($row = $result->fetch_assoc()) {
					$today = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
					$tomorrow = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
					$return[$i] = $row;
					$return[$i]['master'] = $this->User->get(['id' => $row['master']]);
					++$i;
				}

				return $return;
				break;
			}

			case 'type': {
				$sql = "SELECT * FROM request WHERE $request->param = '$request->value'";
				$result = $this->db->query($sql);
				$row = $result->fetch_assoc();
			
				if($row['vendor'] > 0)
					$row['vendor'] = $this->User->get([ 'param' => 'id', 'value' => $row['vendor'] ]);

				return $row; 
				break;
			}

			default: {
				if(!$request->id)
					return false;
					
				$sql = "SELECT * FROM request WHERE id = '$request->id'";
				$result = $this->db->query($sql);
				$row = $result->fetch_assoc();
				
				if($row['vendor'] > 0)
					$row['vendor'] = $this->User->get([
						'param' => 'id',
						'value' => $row['vendor']
					]);

				return $row;
			}
		}


	}

	/* CHANGE REQUEST */
	public function change($data = false) {
		$request = new stdClass();
		$request->id = $data['id'] ?? false;
		$request->param = $data['param'] ?? false;
		$request->value = $data['value'] ?? NULL;
		$request->current = $this->get(['id' => $request->id]);
		$request->time = $data['time'] ?? 15;

		if(!$request->id || !$request->param)
			return false;
		else if($request->current['status'] > 2 && ($request->param === 'start' || $request->param === 'end'))
			return false;

		if($request->value == NULL || empty($request->value))
			$sql = "UPDATE request SET $request->param = NULL WHERE id = '$request->id'";
		else
			$sql = "UPDATE request SET $request->param = '$request->value' WHERE id = '$request->id'";

		if(!$this->db->query($sql)) {
			$return->error = $this->db->error;
			return $return;
		}

		if($request->value == NULL || empty($request->value)) {
			$request->action = 'clear '.$request->param;
		} else {
			switch($request->param) {
				case 'status': {
					$request->action = $this->status($request->value);
					$request->status = $this->status($request->value);
		
					// Convert date into US format
					$request->data = date('F j, Y', strtotime($request->current['date']));
		
					// Notify users
					if(
						$request->current['status'] == NULL && $request->value == 1
						|| $request->current['status'] == 6 && $request->value == 1
						) {
						$message = 'Hi, ' . $request->current['name'] . ', this is ' . $request->current['vendor']['company_name'] . ', your order #'. $request->id .' for ' . $request->data . ' has been approved. For more information call us on this phone number back.';
						$this->Bird->send(['type' => 'voice', 'to' => $request->current['phone'], 'message' => $message]);
					} else if($request->value == 3) {
						$message = 'Hi, ' . $request->current['name'] . ', your master will arrive on ' . $request->data . ' between ' . $request->current['start'] . ' and ' . $request->current['end'] . '. For more information call us on this phone number back.';
						$this->Bird->send(['type' => 'voice', 'to' => $request->current['phone'], 'message' => $message]);
					} else if($request->value == 4) {
						$message = 'Hi, ' . $request->current['name'] . ', master will arrive in ' . $request->time . ' minutes';
						$this->Bird->send(['type' => 'voice', 'to' => $request->current['phone'], 'message' => $message]);
					} else if($request->value == 6) {
						$message = 'Hi, ' . $request->current['name'] . ', you order #'. $request->id .' for ' . $request->data . ' was canceled. For more information call us on this phone number back.';
						$this->Bird->send(['type' => 'voice', 'to' => $request->current['phone'], 'message' => $message]);
						
						
						$master = $this->User->get([
							'param' => 'id',
							'value' => $request->current['master']
							]);
						$message = 'Order #'. $request->id .' for ' . $request->data . ' was canceled.';
						$this->Bird->send([
							'to' => $master['phone'], 
							'message' => $message
						]);
					}
					break;
				}

				case 'master': {
					$row = $this->User->get(['param' => 'id', 'value' => $request->value]);
					$request->action = ''.$request->param.': ' . $row['fname'] . ' ' . $row['lname'];
					break;
				}

				default: {
					$request->action = ''.$request->param.': '.$request->value;
				}
			}
		}
			

		if(
			$request->param != 'position'
			&& $request->param != 'lat'
			&& $request->param != 'lng'
			&& $request->param != 'distance'
		) {
			$this->action([
				'type' => 'request',
				'request' => $request->id,
				'action' => $request->action,
				'status' => $request->status ?? NULL,
				'comment' => $data['comment'] ?? NULL,
				'param' => $request->param,
				'value' => $request->value
			]);
		}

		return true;
	}



	public function create($data = false) {
		$request = new stdClass();

		$request->user = $data['user'] ?? false;
		$request->phone = $data['phone'] ?? false;

		if(!$request->user) {
			$userData = $this->User->get([
					'param' => 'phone',
					'value' =>	$request->phone
			]);
		} else {
			$userData = $this->User->get([
				'id' => $data['user']
			]);
		}

		$request->user = $userData['id'];
		$request->vendor = $data['vendor'] ?? $userData['vendor'];

		$request->category = $data['category'] ?? false;
		$request->name = $data['fname'].' '.$data['lname'] ?? NULL;
		$request->address = $data['address'] ?? NULL;
		$request->utc = $data['utc'] ?? NULL;
		$request->comment = $data['comment'] ?? NULL;
		$request->subcomment = $data['subcomment'] ?? NULL;
		$request->lat = $data['lat'] ?? NULL;
		$request->lng = $data['lng'] ?? NULL;

		/* Additional product data */
		$request->product = $data['product'] ?? NULL;
		$request->dd = $data['dd'] ?? NULL;
		$request->sc = $data['sc'] ?? NULL;


		$sql = "INSERT INTO request (
			category,
			vendor,
			phone,
			name,
			address,
			comment,
			subcomment,
			lat,
			lng,
			product,
			dd,
			sc,
			user,
			utc
		) VALUES (
			'$request->category',
			'$request->vendor',
			'$request->phone',
			'$request->name',
			'$request->address',
			'$request->comment',
			'$request->subcomment',
			'$request->lat',
			'$request->lng',
			'$request->product',
			'$request->dd',
			'$request->sc',
			'$request->user',
			'$request->utc'
		)";

		if(!$this->db->query($sql))
			return $request->error = $this->db->error;

		$request->id = $this->db->insert_id;
		$this->action([
			'type' => 'request',
			'request' => $request->id,
			'action' => 'create',
			'status' => 'new'
		]);

		return $request->id;
	}


	/* Function description */
	public function remove($data = false) {
		$request = new stdClass();
		$request->id = $data['id'] ?? false;

		if(!$request->id)
			return false;


		$sql = "SELECT * FROM request WHERE id = '$request->id'";
		$result = $this->db->query($sql);
		$row = $result->fetch_assoc();

		$request->phone = $row['phone'];

		if (
			$this->type != 'admin'
			&& $row['user'] != $this->id
			&& $row['vendor'] != $this->id
		)
			return false;


		$sql = "DELETE request, action FROM request
				INNER JOIN request_action AS action
				ON request.id = action.request
				WHERE request.id = '$request->id'";

		if(!$this->db->query($sql))
			$return['error'] = $this->db->error;

		$this->action([
			'type' => 'request',
			'request' => $request->id,
			'action' => 'remove',
			'param' => 'remove'
		]);

		return $return;
	}



	public function cancel($phone = false) {
		$phone = str_replace('+', '', $phone); 
		$phone = preg_replace('/\s+/', '', $phone);

		$request = $this->get([
			'type' => 'type',
			'param' => 'phone',
			'value' => $phone
		]);

		$sql = "UPDATE request SET status = '6' WHERE phone = '$phone'";
		if(!$this->db->query($sql))
			return $this->db->error;

		$date = date_create($request['date']);
		$request['date'] = date_format($date, "M d, Y");
	
		$message = 'Hi, ' . $request['name'] . ', you order #'. $request['id'] .' for ' . $request['date'] . ' was canceled. For more information call us on this phone number back.';
		$this->Bird->send([ 'phone' => $phone, 'message' => $message ]);
		return true;
	}


	public function history($data = false) {
		$request = new stdClass();
		$request->id = $data['id'] ?? false;

		if(!$request->id)
			return false;

		$sql = "SELECT *
			FROM request_action
			WHERE request = '$request->id'
						AND param != 'disable'
						AND param != 'start'
						AND param != 'end'
			ORDER BY time DESC LIMIT 100";
		$result = $this->db->query($sql);
		$i = -1;
		while($row = $result->fetch_assoc()) {
			$return[++$i] = $row;

			if($row['action'] === 'cancel') {
				$sqlMedia = "SELECT * FROM request_files
								WHERE request = '$request->id'
									AND type = 'cancel'";
				$resultMedia = $this->db->query($sqlMedia);
				$return[$i]['media'] = [];

				if($resultMedia->num_rows > 0) {
					$i2 = -1;
					while($media = $resultMedia->fetch_assoc()) {
						$return[$i]['media'][++$i2] = $media;
					}
				}
			}
		}

		return $return ?? NULL;
	}

}
