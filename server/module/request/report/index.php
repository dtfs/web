<?php
class Report extends App {

	public function __construct() {
		parent::__construct();
	}

	public function get($data = false) {
		$report = new stdClass();
		$report->request = $data['request'] ?? false;

		if(!$report->request)
			return false;

		$sql = "SELECT * FROM request_report
				WHERE request = '$report->request'";
		$result = $this->db->query($sql);

		if ($result->num_rows > 0) {
		 	$row = $result->fetch_assoc();
		} else {
			$sqlCreate = "INSERT INTO request_report (request) VALUES ('$report->request')";
			if(!$this->db->query($sqlCreate))
					return $this->db->error;

			$result = $this->db->query($sql);
			$row = $result->fetch_assoc();
		}

		$sqlFiles = "SELECT * FROM request_files WHERE request = '$report->request' AND type = 'report'";
		$Files = $this->db->query($sqlFiles);

		if($Files->num_rows > 0) {
			$i = -1;
			while($file = $Files->fetch_assoc()) {
				$row['files'][++$i] = $file;
			}
		}

		return $row;
	}


	public function change($data = false, $return = false) {
		$report = new stdClass();
		$report->request = $data['request'] ?? false;

		if(!$report->request)
			return false;

		$report->id = $this->get(['request' => $report->request]);
		$report->data = $data['data'];

		$sql = "UPDATE request_report SET data = '$report->data' WHERE request = '$report->request'";
		if(!$this->db->query($sql))
			return $this->db->error;

		return $report->id;
	}

	public function file($data = false) {

	}


}
