<?php

class Cancel extends App {

	public function __construct() {
    	parent::__construct();
  	}

	public function get($data = false) {
		$return = new stdClass();
		$cancel = new stdClass();
		$cancel->request = $data['request'];

		$sql = "SELECT * FROM request_files
						WHERE request = '$cancel->request'
							AND type = 'cancel'";
		$result = $this->db->query($sql);

		if(!$this->db->query($sql))
			$return->error = $this->db->error;

		if ($result->num_rows > 0) {
			$i = -1;
			while($row = $result->fetch_assoc()) {
				$return->data[++$i] = $row;
			}

			return $return;
		}

		return false;
	}
}
