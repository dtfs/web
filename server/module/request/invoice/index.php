<?php

class Invoice extends App {
	public function __construct() {
    	parent::__construct();
		$this->Request = $this->autoload('request');
    }

	public function get($data = false) {
		$report = new stdClass();
		$report->request = $data['request'] ?? false;
		$report->from = $data['from'] ?? false;
		$report->to = $data['to'] ?? false;
		$report->vendor = $data['vendor'] ?? false;
		$report->master = $data['master'] ?? false;

		if($report->from && $report->to && ($report->vendor || $report->master)) {
            $sql = "SELECT i.*
                FROM request_invoice AS i
                LEFT JOIN request AS r
                    ON i.request = r.id
                WHERE (r.vendor = '$report->vendor' OR r.master = '$report->master') 
                    AND (i.status = 2 OR i.status = 4)
                    AND i.date BETWEEN '$report->from' AND '$report->to'";
            $result = $this->db->query($sql);

            $i = -1;
            while($row = $result->fetch_assoc()) {
                $return[++$i] = $row; 
                $return[$i]['report'] = json_decode($row['report']);
                $return[$i]['price'] = $row['price'] / 100;
                $return[$i]['request'] = $this->Request->get([ 'id' => $row['request'] ]);
            }

            $sql = "UPDATE request_invoice AS i
                LEFT JOIN request AS r
                    ON i.request = r.id
                SET i.status = 4
                WHERE (r.vendor = '$report->vendor' OR r.master = '$report->master') 
                    AND (i.status = 2 OR i.status = 4)
                    AND i.date BETWEEN '$report->from' AND '$report->to'";
 
            if(!$this->db->query($sql))
                return $this->db->error;
        } else if($report->request) {
            $return->request = $this->Request->get([ 'id' => $report->request ]);
            $sql = "SELECT * FROM request_invoice
                    WHERE request = " . $return->request['id'];
            $result = $this->db->query($sql);

            if ($result->num_rows > 0) {
                 $return = $result->fetch_assoc();
            } else {
                $sqlCreate = "INSERT INTO request_invoice (
                        request, master, date
                    ) VALUES (
                        '$report->request',
                        '" . $return->request['master'] . "',
                        '" . $return->request['date'] . "'
                    )";
    
                if(!$this->db->query($sqlCreate))
                    return $this->db->error;
    
                $result = $this->db->query($sql);
                $return = $result->fetch_assoc();
            }
        }

        

		return $return;
	}

	public function change($data = false, $return = false) {
		$report = new stdClass();
		$report->request = $data['request'] ?? false;
        $report->data = $data['report'] ?? false;


		if(!$report->request || !$report->data)
            return false;
            
        $report->stringify = json_encode($report->data); 
        $report->request = $this->Request->get([ 'id' => $report->request ]);
        $report->servicePrice = $this->Request->User->Price->get([ 
            'id' => $report->data['service'], 
            'user' => $report->request['master'] 
        ]); 

        $report->service = $report->servicePrice['id'];
        $report->service_name = $report->servicePrice['name'];
        $report->price = $report->servicePrice['price'];


		$sql = "UPDATE request_invoice 
            SET report = '$report->stringify',
                price = '$report->price',
                service = '$report->service',
                service_name = '$report->service_name'
            WHERE request = " . $report->request['id'];

		if(!$this->db->query($sql))
            return $this->db->error;
        
        $request = $this->get([ 'request' => $report->request['id'] ]);
        $report->data['id'] = $request['id'];
        $report->data['master'] = $request['master'];
        $report->data['service'] = $report->service;
        $report->data['service_name'] = $report->service_name;
        $report->data['price'] = $report->price / 100;
		return $report->data;
	}
      

    public function miles($data = false) {
        $invoice = new stdClass();
        $invoice->master = $data['master'];
        $invoice->date = $data['date'];
        $invoice->miles = $data['miles'];
        $invoice->baseMiles = 0;  
        $invoice->price = ($data['miles'] - $invoice->baseMiles) * .45 * 100;
        $invoice->service = 0;
        $invoice->service_name = 'miles';
        $master = $this->Request->User->get([
            'param' => 'id',
            'value' => $invoice->master
        ]);
        
        $sql = "SELECT * FROM request_invoice 
            WHERE master = '$invoice->master' 
                AND date = '$invoice->date' 
                AND request IS NULL  
                AND service = '$invoice->service'";
        $result = $this->db->query($sql);
        
        if ($result->num_rows > 0) {
			$sqlUpdate = "UPDATE request_invoice 
                SET miles = '$invoice->miles',
                    price = '$invoice->price'
                WHERE master = '$invoice->master' 
                    AND date = '$invoice->date'
                    AND request IS NULL  
                    AND service = '$invoice->service'";

            if(!$this->db->query($sqlUpdate))
                return $this->db->error;

            $result = $this->db->query($sql);
            $row = $result->fetch_assoc();
        } else {
            $sqlCreate = "INSERT INTO request_invoice (
                    price, service, service_name, master, date, miles
                ) VALUES (
                    '$invoice->price',
                    '$invoice->service',
                    '$invoice->service_name',
                    '$invoice->master',
                    '$invoice->date',
                    '$invoice->miles'
                )";

            if(!$this->db->query($sqlCreate))
                return $this->db->error;

            $result = $this->db->query($sql);
            $row = $result->fetch_assoc();
        }
        
		$invoice->date = date('F j, Y', strtotime($invoice->date));
        $this->Request->Bird->send([
            'phone' => $user->phone,
            'message' => $master['fname'] .  ', your road for ' . $invoice->date . ' is ready. Set time-frame in planner before 8pm or it will be set automaticly.'
        ]);

        return $row;
    }

    public function pay($data = false) {
		$report = new stdClass();
		$report->request = $data['request'] ?? false;
        $report->id = $data['id'] ?? false;

        $sql = "UPDATE request_invoice 
            SET status = '2'
            WHERE id = '$report->id'";

        if(!$this->db->query($sql))
            return $this->db->error;

        return true;
    }

    public function cancel($data = false) {
		$report = new stdClass();
		$report->request = $data['request'] ?? false;
        $report->id = $data['id'] ?? false;

        $sql = "UPDATE request_invoice 
            SET status = '3'
            WHERE id = '$report->id'";

        if(!$this->db->query($sql))
            return $this->db->error;

        return true; 
    }
}
