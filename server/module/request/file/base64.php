<?php
$base64 = $_POST['base64'] ?? false;
$name = $_POST['name'] ?? 'download';

if($base64) { 
    $pdf_decoded = base64_decode($base64);
    $pdf = fopen($_SERVER["DOCUMENT_ROOT"].'/server/module/request/file/cache/' . $name . '.pdf', 'w');
    fwrite ($pdf,$pdf_decoded);
    fclose ($pdf);
    echo true;
} else {
    unlink($_SERVER["DOCUMENT_ROOT"].'/server/module/request/file/cache/' . $name . '.pdf');
    echo true;
}
