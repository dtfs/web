<?php
/* IMPORT MAIN APP */
require_once $_SERVER["DOCUMENT_ROOT"].'/server/module/app.php';
$app = new App();
/* IMPORT MAIN APP */

/* IMPORT SENTRY DEBUG */
require_once $_SERVER["DOCUMENT_ROOT"].'/server/vendor/autoload.php';
Raven_Autoloader::register();
$client = new Raven_Client('https://0ff9dad000ff4215af9b604ac3c66f79@sentry.io/282807');
$error_handler = new Raven_ErrorHandler($client);
$error_handler->registerExceptionHandler();
$error_handler->registerErrorHandler();
$error_handler->registerShutdownFunction();
/* IMPORT SENTRY DEBUG */

/* IMPORT REQUEST */
$request = $app->autoload('request');
/* IMPORT REQUEST */


$sql = "SELECT * FROM request
	WHERE date_format(date, '%Y-%m-%d') < date_format(now(), '%Y-%m-%d')
		AND status = 2
		AND master IS NOT NULL";
$result = $app->db->query($sql);

while($row = $result->fetch_assoc()) {
	$request->change([
		'id' => $row['id'],
		'param' => 'status',
		'value' => 3,
		'comment' => 'System set status ready'
	]);
}

$sql = "SELECT * FROM request
	WHERE date_format(date, '%Y-%m-%d') < date_format(now(), '%Y-%m-%d')
		AND status < 2
		AND master IS NOT NULL";
$result = $app->db->query($sql);

while($row = $result->fetch_assoc()) {
	$request->change([
		'id' => $row['id'],
		'param' => 'status',
		'value' => 6,
		'comment' => 'System set status cancel'
	]);
}
