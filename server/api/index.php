<?php
if(
	(
		$_POST 
		&& !empty($_POST['get']) 
		&& !empty($_POST['do'])
	) || (
		$_GET 
		&& !empty($_GET['get']) 
		&& !empty($_GET['do'])
	)
) {
	require_once $_SERVER["DOCUMENT_ROOT"].'/server/vendor/autoload.php';
	
	/* IMPORT MAIN APP */
	require_once $_SERVER["DOCUMENT_ROOT"].'/server/module/app/index.php';
	$app = new App();
  	/* IMPORT MAIN APP */

  	/* IMPORT SENTRY DEBUG *
	Raven_Autoloader::register();
	$client = new Raven_Client('https://0ff9dad000ff4215af9b604ac3c66f79@sentry.io/282807');
	$error_handler = new Raven_ErrorHandler($client);
	$error_handler->registerExceptionHandler();
	$error_handler->registerErrorHandler();
	$error_handler->registerShutdownFunction();
	/* IMPORT SENTRY DEBUG */

  	header('Content-type: application/json');

	$class = $_POST['get'] ?? $_GET['get'];
	$do = $_POST['do'] ?? $_GET['do'];
	$data = $_POST['data'] ?? $_GET['data'] ?? null;

	if($class == 'app')
		$return = $app->$do($data);
	else {
		$action = $app->autoload($class);
		$return = $action->$do($data);
	}

  	echo json_encode($return);
}
