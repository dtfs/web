#!/usr/bin/env nodejs
 
const Sentry = require('@sentry/node')
Sentry.init({ dsn: 'https://0ff9dad000ff4215af9b604ac3c66f79@sentry.io/282807' })

const WebSocketServer = require('ws').Server
const httpServ = require('https')
const fs = require('fs')
const cfg = {
     ssl: true,
     port: 8080,
     ssl_key: '/home/dtfs.pro.key',
     ssl_cert: '/home/dtfs.pro.crt'
 }

const processRequest = function( req, res ) {
 res.writeHead(200)
 res.end("All glory to WebSockets!\n")
}

const app = httpServ.createServer({
	key: fs.readFileSync( cfg.ssl_key ),
	cert: fs.readFileSync( cfg.ssl_cert )
}, processRequest ).listen( cfg.port )

const wss = new WebSocketServer({ server: app })
const clients = []


wss.on('connection', wss => {
	const id = (Math.random() * Math.random()) / Math.random()
	clients[id] = wss
	clients[id].send(JSON.stringify({
		action: 'connection',
		id
	}))

	wss.on('message', message =>  {
		message = JSON.parse(message)
		console.log(message)

		switch(message.type) {
			case 'account': {
				clients[message.connection]['account'] = message.value
				clients[message.connection]['account_id'] = message.id
				break
			}
		}

		for (const key in clients) {
			if(key != id) {
				if(clients[key]['account'] === 'admin') {
					clients[key].send(JSON.stringify(message))
				} else {
					if(message[clients[key]['account']] == clients[key]['account_id']) {
						clients[key].send(JSON.stringify(message))
					}
				}
			}
		}
	})

	wss.on('close', _ => {
		console.log('соединение закрыто ' + id)
		delete clients[id]
	})

})
